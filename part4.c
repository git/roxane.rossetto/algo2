#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include"part4.h"


Filechx initchx (void){// initialise une file choix a NULL
	Filechx f;
	f.kechx = NULL;
	f.ttchx = NULL;
	return f;
}


Filecand initcand (void){// initialise une file candidat a NULL
	Filecand f;
	f.kecand = NULL;
	f.ttcand = NULL;
	return f;
}

int testVideCand (Filecand f){// fonction test si la Filecand est vide return 1 sinon 0
	if (f.ttcand == NULL) return 1;
	return 0;
}

int testVideChx (Filechx f){// fonction test si la Filechx est vide return 1 sinon 0
	if (f.ttchx == NULL) return 1;
	return 0;
}

Candidat lireCa(FILE *fe)// lit un candidat dans un fichier
{
	Candidat c;//candidat que l'on retournera
	int cpt;//pour la création de maillons
	fscanf(fe,"%d", &c.nEtu);
	fscanf(fe, "%s%*c", c.nom);

	fgets(c.prenom, 27, fe);
	c.prenom[strlen(c.prenom)-1]= '\0';

	fscanf(fe, "%f %f %f %f %f", &c.moymat, &c.moyfr, &c.moyen, &c.moyspe, &c.noteDoss);
	
	fscanf(fe, "%d", &c.nbchx);
	c.fchx = initchx();//initialisation de la file
	for (cpt = 0; cpt < c.nbchx; cpt++)
	{
		//puts("je passe ici aussi\n");	
		Choix chx;
		chx = lireC(fe);
		c.fchx = enf(c.fchx, chx);
	}

	return c;
}

/*
// FAIRE LECTURE CHOIX POUR METTRE EN PARAMETRE PLUTOT QUE LE FE

Filechx enfilechx ( Filechx f , FILE *fe){//fonction enfile un maillon choix en fin de file Choix
	Maillonchx *mch;
	int nbchoix, i, nbcand, numCand;
	char nom[30], prenom[30];
	float nmat, nfr, nen, nspe, nDoss;

	mch = (Maillonchx*) malloc ( sizeof (Maillonchx));
	if ( mch == NULL ){
		printf("Probleme malloc");
		exit(-1);
	}
	fscanf ( fe , "%d %d %s %s %f %f %f %f %f %d", &nbcand, &numCand, nom, prenom, &nmat, &nfr, &nen, &nspe, &nDoss, &nbchoix );
	for ( i = 0 ; i < nbchoix ; i++ ){
		fscanf( fe, "%s %s %d %d", mch->chx.ville, mch->chx.dptmt, mch->chx.dec, mch->chx.valid);
		if ( testVideChx (f)){
			f.ttchx = mch;
			f.kechx = mch;
		}
		else{
			f.kechx->suivchx = mch;
			f.kechx = mch
		}
	}
	return f;
}
*/

//celle là c'est la bonne pour enfiler dans une file de choix
Filechx enf(Filechx fchx, Choix c)
{
	Maillonchx *m;
	m = (Maillonchx*)malloc(sizeof(Maillonchx));
	if (m == NULL){printf("err pb malloc maillon choix\n");exit(-1);}
	strcpy(m->chx.ville, c.ville);
	strcpy(m->chx.dptmt, c.dptmt);
	m->chx.dec = c.dec;
	m->chx.valid = c.valid;
	m->suivchx = NULL;
	if (testVideChx (fchx))
	{
		fchx.ttchx = m;
		fchx.kechx = m;
	}
	else
	{
		fchx.kechx->suivchx = m;
		fchx.kechx = m;
	}
	return fchx;
}





Filecand enfC(Filecand fC, Candidat c)
{
	Mailloncand *m;
	int cpt;
	m = (Mailloncand *)malloc(sizeof(Mailloncand));
	if (m == NULL){printf("pb malloc\n"); exit(-1);}
	m->cand.nEtu = c.nEtu;
	strcpy(m->cand.nom , c.nom);
	strcpy(m->cand.prenom, c.prenom);
	m->cand.moymat = c.moymat;
	m->cand.moyfr = c.moyfr;
	m->cand.moyen = c.moyen;
	m->cand.moyspe = c.moyspe;
	m->cand.noteDoss = c.noteDoss;
	m->cand.nbchx = c.nbchx;
	m->cand.fchx = initchx();
	for (cpt = 0; cpt < c.nbchx; cpt++)
	{
		if (c.fchx.ttchx == NULL) break;
		m->cand.fchx = enf(m->cand.fchx, c.fchx.ttchx->chx);
		c.fchx.ttchx = c.fchx.ttchx->suivchx;
	}
	m->suivcand = NULL;
	if (testVideCand(fC))
	{
		fC.ttcand = m;
		fC.kecand = m;
	}
	else
	{
		fC.kecand->suivcand = m;
		fC.kecand = m;
	}
	return fC;

}


Choix def(Filechx fc)
{
	Maillonchx *tmp;

	if (testVideChx){printf("file vide impossible de défiler\n");exit(-1);}
	else
	{
		Choix c;
		c = fc.ttchx->chx;//on met le choix de la file dans le choix à retourner
		tmp = fc.ttchx->suivchx;//on crée le maillon temporaire qui prend la valeur du suivant
		free(fc.ttchx);//on free celui qui doit être défilé
		fc.ttchx=tmp;//on remet la file comme il faut
		return c;
	}
}


Candidat defC(Filecand fC)//même version mais avec un candidat
{
	Mailloncand	*tmp;

	if (testVideCand){printf("file vide impossible de défiler\n");exit(-1);}
	else
	{
		Candidat c;
		c = fC.ttcand->cand;
		tmp = fC.ttcand->suivcand;
		free(fC.ttcand);
		fC.ttcand = tmp;
		return c;
	}
}




/*
// FAIRE LECTURE CANDIDAT POUR METTRE EN PARALLELE

Filecand enfilecand (Filecand f, FILE *fe){// fonction enfile un maillon cand en fin de file Candidat
	Mailloncand *mc;
	int nbchoix, i nbcand;

	mc = (Mailloncand*) malloc (sizeof (Mailloncand));
	if ( mc == NULL ){
		printf("probleme malloc");
		return -1;
	}
	mc->cand = lireCa (fe);
	//fscanf(fe, "%d %d %s %s %f %f %f %f %f %d",&nbcand, &mc->cand.nEtu, mc->cand.nom, mc->cand.prenom, &mc->cand.moymat, &mc->cand.moyfr, &mc->cand.moyen, &mc->cand.moyspe, &mc->cand.noteDoss, &nbchoix);
	mc->suivcand = NULL;
	if ( testVideCand (f)){
		f.ttcand = mc;
		f.kecand = mc;
	}
	else{
		f.kecand->suivant = mc;
		f.kecand = mc;
	}
	return f;
}

int chargement (void){// chargement des files cand et chx

}
*/



Choix lireC(FILE *fe)//fonction extraite de la partie 2
{
	Choix c;
  	fscanf(fe, "%s%*c", c.ville);
	fgets(c.dptmt, 26, fe);
	c.dptmt[strlen(c.dptmt)-1]= '\0';
	fscanf(fe, "%d %d%*c", &c.dec, &c.valid);
	return c;
}



/*gardant ce modèle il nous faudra faire le chargement de files
Listecand Chargementlistecandidat(FILE *fe, Listecand lC, int *nbC)// fonction de chargement de la liste des candidats
{
	int cpt, j;
	fscanf(fe, "%d", nbC);
	for (j = 1; j <= *nbC; j++)
	{
    Candidat Ca;
		fscanf(fe, "%d %s%*c", &Ca.nEtu, Ca.nom);
  	fgets(Ca.prenom, 26, fe);
  	Ca.prenom[strlen(Ca.prenom)-1]= '\0';
    fscanf(fe,"%f %f %f %f %f %d%*c", &Ca.moymat, &Ca.moyfr, &Ca.moyen, &Ca.moyspe, &Ca.noteDoss, &Ca.nbchx);
    Ca.lchx=NULL;

		for (cpt = Ca.nbchx; cpt >= 1; cpt--)
		{
			Choix c;
			c = lireC(fe);//lire le maillon avec la fonction plus haut
			Ca.lchx = InsertC(Ca.lchx, c);//insert le maillon à sa bonne place
		}
		lC = Insert(lC, Ca);
	}
	return lC;
}
*/
Filecand ChargementfileC(Filecand fC, int *nbC)//chargement du fichier dans cette liste de candidats avec le nombre nbC de candidats à lire dans le fichier
{
	FILE *fe;
	int cpt;
	fe = fopen("part4.don", "r");
	if (fe == NULL){printf("pb ouv part4.don\n"); exit(-1);}
	fscanf(fe, "%d", nbC);
	for (cpt = 0; cpt < *nbC; cpt ++)
	{	
		Candidat c;
		c = lireCa(fe);
		fC = enfC(fC, c);
	}
	fclose(fe);
	return fC;
}

int chargement (Candidat *tCandAcc[], int tmax, FILE *fe){// chargement des files cand et chx
	int i, nbCand;
	Candidat c;
	Choix chx;

    fscanf( fe, "%d", &nbCand );

    c = lireCa (fe);

    while ( !feof(fe) ){
        if ( i == tmax){
            printf("La liste des candidats admis est complète\n");
            return 0;
        }
        tCandAcc[i] = (Candidat*) malloc (sizeof(Candidat));
        if (tCandAcc[i] == NULL ){
            printf("Problème malloc\n");
            return -1;
        }
        tCandAcc[i] = c;
        fscanf( fe, "%d", &nbC);
        for ( i=0 ; i<nbc ; i ++){
        	chx = lireC( fe );
        	tCandAcc[i]->lchx = enf( tCandAcc[i]->lchx, chx );

        }
        i++;
        c = lireCa (fe);
    }
    fclose (fe);
    return i;
}

void test (void){
	FILE *fe;
	int menu1, tlog;
    Candidat *tCand[120];

	fe = fopen( "part4.don", "r");
	if ( fe == NULL ){
		printf(" probleme d'ouverture de fichier");
		return;
	}
	Filecand fC;
	int nbC;
	fC = initcand();
	fC = ChargementfileC(fC, &nbC);

    tlog = chargement (tCand, 120, fe);

    Afftab(tCand, tlog);

    /*
    printf("Etes-vous un candidat(1) ou un responsable(2)?\n");
    scanf("%d", &menu1);

    while( menu1 == 1 || menu1 == 2 ){

        if ( menu1 == 1 ){

        }
    }
    */

}

