typedef struct {
  char ville[26];//ville choisie
  char dptmt[26];//département choisi
  int dec;//décision du choix
  int valid;//validation du candidat
}Choix;


typedef struct Listchx{
  Choix chx;
  struct Listchx * suivchx;
}Maillonchx;

typedef Maillonchx * Listechx;


typedef struct {
  int nEtu;//numéro d'étudiant
  char nom[26];//nom de l'étudiant
  char prenom[26];//prénom de l'étudiant
  float moymat;//moyenne en maths
  float moyfr;//moyenne en français
  float moyen;//moyenne en anglais
  float moyspe;//moyenne en spécialité
  float noteDoss;//note après étude du dossier
  int nbchx;//nombre de choix
  Listechx lchx;
}Candidat;


typedef struct Mailloncand{
  Candidat cand;
  struct Mailloncand * suivcand;
}Mailloncand;

typedef Mailloncand * Listecand;


Choix lireC(FILE *fe);//fonction extraite de la partie 2
Listechx InsertTC(Listechx list, Choix m);//Insert en tête de la liste
Listechx InsertC(Listechx list, Choix m);//insert globalement dans liste de choix
Listecand InsertT(Listecand list, Candidat c);//Insert en tête de la liste
Listecand Insert(Listecand list, Candidat c);//insert globalement
Listecand InsertN(Listecand list, Candidat c);//insert globalement en fonction de la note
Listecand Chargementlistecandidat(FILE *fe, Listecand lC, int *nbC);// fonction de chargement de la liste des candidats
Listecand AcceptedOrWait(Listecand lC, int *nbC, float noteMin);
void Save(Listecand lC, int nbC);//fonction de sauvegarde globale
void saveC(Listecand lC, FILE *fs);//sauvegarde un candidat
void saveChx(Listechx lCh, FILE *fs);//sauvegarde tous les choix d'un candidat
void RespAdmin(void);//fonction dédiée au responsable d'admission
void AffC(Candidat c);
void AffCh(Choix c);
void AffListeCandidat (Listecand l);
void AffListChoix(Listechx l);
void MajDecJury(Listecand lC, int nbC, int nbAcc, int *nbCacc, int *nbCW);//mise à jour de la décision du jury
Listecand searchCand(Listecand lC, char *nom, char *prenom);//recherche d'un candidat par son nom et prénom
Listecand MajNote(Listecand lC, char *nom, char *prénom, float newNote);//mise à jour de la note de dossier du candidat
Listecand splitAccepted(Listecand origin);//fonction qui retourne la liste des admis
Listecand splitWait(Listecand origin);//fonction qui retourne la liste d'attente
