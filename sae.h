


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*-------------------------------------------------------part1.h------------------------------------------------------*/




typedef struct list{
	char departement[31];//le nom du département
	int nbP;//nombre de places
	char respAd[31];//nom du responsable
	struct list * suiv;//pointeur suivant de la liste
}MaillonDept;


typedef struct{
	char ville[30];//nom de la ville
	int nbDept;//nombre de départements
	MaillonDept * ldept;//liste de départements
}VilleIUT;

typedef MaillonDept * ListDept;


ListDept init(void);//fontion initialisation
MaillonDept lireM(FILE *f);//lire maillon
int Exists(char *st1, char *st2);// servira à trouver si le nom de la ville existe déjà donc il suffira d'ajouter un maillon à la chaîne
ListDept InsertT1(ListDept list, MaillonDept m);//Insert en tête de la liste
ListDept Insert1(ListDept list, MaillonDept m);//insert globalement
int Chargement(VilleIUT **tV, int tmax, char *nameFile);//charge le fichier dans le tableau
void AfficheDpmt ( MaillonDept *dpt );//Affiche un département d'une liste
void afficheIUT (VilleIUT *iut);//Affiche un iut avec ses départements
void Affichetableau(VilleIUT ** TabIUT, int n );//Affiche le tableau des iuts avec leurs départements
ListDept recherche (ListDept l, char *nom );//recherche du nom de département dans la liste
void MaJnbP(VilleIUT **tV, int nb);//mise à jour du nombre de places
int searchVille(VilleIUT ** tV, char *ville, int nbV);//recherche du nom de la ville dans le tableau
void createIUT(VilleIUT **tV, int nb);//crée un IUT dans la ville souhaitée.
MaillonDept lireStdin(void);
ListDept SuppT(ListDept ld);//supprime la tête d'une liste
ListDept Supp(ListDept ldp, char *nomdpt);//supprime normal
void deleteIUT(VilleIUT **tV, int nb);//crée un IUT dans la ville souhaitée.
void MaJnameC(VilleIUT **tV, int nb);//mise à jour du nom du chef de département
void Save1(VilleIUT **tV, int nb);//sauvegarde le fichier
void SaveEltList(FILE *fs, ListDept l);//sauvegarde un élément de liste
int Admin(VilleIUT **tV, int nb);//partie admin de la première partie
void AffnbPdpt ( MaillonDept *dpt );//Affiche nombre de place par département d'une liste
void affichenbPIUT (VilleIUT *iut);//Affiche nombre de place restante d'un iut avec ses départements
void AffichenbPtableau(VilleIUT ** TIUT, int n);//Affiche le tableau de places restantes des iuts avec leurs départements
int utilisateur (VilleIUT **tV, int nb);//Onglet accueille des utilisateurs
void part1(void); // fonction globale




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*-------------------------------------------------------part2.h------------------------------------------------------*/



typedef struct {
  char ville[20];//ville choisie
  char dptmt[20];//département choisi
  int dec;//décision du choix
  int valid;//validation du candidat
}Choix;


typedef struct Listchx{
  Choix chx;
  struct Listchx * suivchx;
}Maillonchx;

typedef Maillonchx * Listechx;


typedef struct {
  int nEtu;//numéro d'étudiant
  char nom[20];//nom de l'étudiant
  char prenom[20];//prénom de l'étudiant
  float moymat;//moyenne en maths
  float moyfr;//moyenne en français
  float moyen;//moyenne en anglais
  float moyspe;//moyenne en spécialité
  int nbchx;//nombre de choix
  Listechx lchx;
}Candidat;


typedef struct Mailloncand{
  Candidat cand;
  struct Mailloncand * suivcand;
}Mailloncand;

typedef Mailloncand * Listecand;


Listechx initch(void);
Listecand initCand(void);
Choix lirechx (void);//permet de lire un choix selon une entrée stdin
Choix lireC(FILE *fe);//ermet de lire un choix avec un FILe de type fichier.don
Candidat lireCandidat(void);//permet de lire un candidat avec le clavier
Candidat lireCa(FILE *fe);//permet de lire un candidat selon le fichier.don
Listechx InsertTC(Listechx list, Choix m);//insertion en tête d'un maillon de type choix
Listechx InsertC(Listechx list, Choix m);//insertion globale d'un maillon de type choix

void AffC(Candidat c);//affichage simple d'un candidat
void AffCh(Choix c);//affichage simple d'un choix
void affnomcand (Candidat c);
void AffListeCandidat (Listecand l);//affiche complet de la liste de candidats
void AffCandid (Listecand l);//affiche que le nom
void AffListChoix(Listechx l);//affichage complet de la liste de choix
Listecand recherchenom (Listecand l, char *nom );//recherche par un nom dans une liste de candidats
Listecand recherchenum (Listecand l, int num);//recherche par le numéro dans une liste de candidats
Listechx searchChx(Listechx lchx, char *ville, char *dptmt);
Listechx recherchechx (Listechx l, char *nom );// recherche par le nom d'un depratement dans une listedederpartement
Listecand recherchevilledpt ( Listecand l, char *ville, char *dpt);//recherche et retour de la liste des candidats ayant postuler a un certain departement
Listechx majchoix (Listechx chx, Choix c);// mise  jour d'un choix
Listecand Chargementlistecandidat(FILE *fe, Listecand lC, int *nbC);//chargement du fichier dans une liste de candidats
Listechx suppressionchxT (Listechx l);//Fonction suppression d'un maillon
Listechx suppressionchx(Listechx l , char *depart);//suppression d'un maillon viale nom de son departement
Listecand Insert(Listecand list, Candidat c);//insertion en tête d'un maillon de type Candidat
Listecand InsertT(Listecand list, Candidat c);//insertion globale d'un maillon de type Candidat
void test (void);//fonction globale
void testchgt(void);
void saveC(Listecand lC, FILE *fs);
void Save(Listecand lC, int nbC);
void saveChx(Listechx lCh, FILE *fs);





////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*-------------------------------------------------------part2.h------------------------------------------------------*/
/*-----------------------------------sans les répétitions entre les différents *.h------------------------------------*/



typedef struct {
  int nEtu;//numéro d'étudiant
  char nom[26];//nom de l'étudiant
  char prenom[26];//prénom de l'étudiant
  float moymat;//moyenne en maths
  float moyfr;//moyenne en français
  float moyen;//moyenne en anglais
  float moyspe;//moyenne en spécialité
  float noteDoss;//note après étude du dossier
  int nbchx;//nombre de choix
  Listechx lchx;
}Candidat;


typedef struct Mailloncand{
  Candidat cand;
  struct Mailloncand * suivcand;
}Mailloncand;

typedef Mailloncand * Listecand;


Choix lireC(FILE *fe);//fonction extraite de la partie 2
Listechx InsertTC(Listechx list, Choix m);//Insert en tête de la liste
Listechx InsertC(Listechx list, Choix m);//insert globalement dans liste de choix
Listecand InsertT(Listecand list, Candidat c);//Insert en tête de la liste
Listecand Insert(Listecand list, Candidat c);//insert globalement
Listecand InsertN(Listecand list, Candidat c);//insert globalement en fonction de la note
Listecand Chargementlistecandidat(FILE *fe, Listecand lC, int *nbC);// fonction de chargement de la liste des candidats
Listecand AcceptedOrWait(Listecand lC, int *nbC, float noteMin);
void Save(Listecand lC, int nbC);//fonction de sauvegarde globale
void saveC(Listecand lC, FILE *fs);//sauvegarde un candidat
void RespAdmin(void);//fonction dédiée au responsable d'admission
void AffC(Candidat c);
void AffCh(Choix c);
void AffListeCandidat(Listecand l);
void AffListChoix(Listechx l);
void MajDecJury(Listecand lC, int nbC, int nbAcc);//mise à jour de la décision du jury
Listecand searchCand(Listecand lC, char *nom, char *prenom);//recherche d'un candidat par son nom et prénom
Listecand MajNote(Listecand lC, char *nom, char *prénom, float newNote);//mise à jour de la note de dossier du candidat
Listecand splitAccepted(Listecand origin);//fonction qui retourne la liste des admis
Listecand splitWait(Listecand origin);//fonction qui retourne la liste d'attente



void global(void);
void main(void);