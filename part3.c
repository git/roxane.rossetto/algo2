#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include"part3.h"

Choix lireC(FILE *fe)//fonction extraite de la partie 2
{
	Choix c;
  fscanf(fe, "%s%*c", c.ville);
	fgets(c.dptmt, 26, fe);
	c.dptmt[strlen(c.dptmt)-1]= '\0';
	fscanf(fe, "%d %d%*c", &c.dec, &c.valid);
	return c;
}


Listecand initC(void)
{
	return NULL;
}


Listechx init(void)
{
	return NULL;
}


Listechx InsertTC(Listechx list, Choix m)//Insert en tête de la liste
{
	Maillonchx *mchx;
	mchx = (Maillonchx*)malloc(sizeof(Maillonchx));
	if (mchx == NULL){printf("pb malloc"); exit(-1);}
	mchx->chx = m;
	mchx->suivchx = list;
	return mchx;
}


Listechx InsertC(Listechx list, Choix m)//insert globalement dans liste de choix
{
	if (list == NULL){return InsertTC(list, m);}
	if (strcmp(list->chx.dptmt, m.dptmt)>0){return InsertTC(list, m);}
	list->suivchx = InsertC(list->suivchx, m);
	return list;
}


Listecand InsertT(Listecand list, Candidat c)//Insert en tête de la liste
{
	Mailloncand *c1;

	c1 = (Mailloncand*)malloc(sizeof(Mailloncand));
	if (c1 == NULL){printf("pb malloc"); exit;}
	c1->cand.nEtu = c.nEtu;
	strcpy(c1->cand.nom, c.nom);
	strcpy(c1->cand.prenom, c.prenom);
	c1->cand.moymat = c.moymat;
	c1->cand.moyfr = c.moyfr;
	c1->cand.moyen = c.moyen;
	c1->cand.moyspe = c.moyspe;
  	c1->cand.noteDoss = c.noteDoss;
	c1->cand.nbchx = c.nbchx;
	c1->cand.lchx = c.lchx;
	c1->suivcand = list;
	return c1;
}


Listecand Insert(Listecand list, Candidat c)//insert globalement
{
	if (list == NULL){return InsertT(list, c);}
	if (strcmp(list->cand.nom, c.nom)>0){return InsertT(list, c);}
	list->suivcand = Insert(list->suivcand, c);
	return list;
}

Listecand InsertN(Listecand list, Candidat c)//insert globalement en fonction de la note
{
	if (list == NULL){return InsertT(list, c);}
	if (list->cand.noteDoss <=+ c.noteDoss){return InsertT(list, c);}
	list->suivcand = InsertN(list->suivcand, c);
	return list;
}


Listecand Chargementlistecandidat(FILE *fe, Listecand lC, int *nbC)// fonction de chargement de la liste des candidats
{
	int cpt, j;
	fscanf(fe, "%d", nbC);
	for (j = 1; j <= *nbC; j++)
	{
    Candidat Ca;
		fscanf(fe, "%d %s%*c", &Ca.nEtu, Ca.nom);
  	fgets(Ca.prenom, 26, fe);
  	Ca.prenom[strlen(Ca.prenom)-1]= '\0';
    fscanf(fe,"%f %f %f %f %f %d%*c", &Ca.moymat, &Ca.moyfr, &Ca.moyen, &Ca.moyspe, &Ca.noteDoss, &Ca.nbchx);
    Ca.lchx = init();

		for (cpt = Ca.nbchx; cpt >= 1; cpt--)
		{
			Choix c;
			c = lireC(fe);//lire le maillon avec la fonction plus haut
			Ca.lchx = InsertC(Ca.lchx, c);//insert le maillon à sa bonne place
		}
		lC = Insert(lC, Ca);
	}
	return lC;
}



void Save(Listecand lC, int nbC)//fonction de sauvegarde globale
{
	FILE *fs;
	//FILE *fs2;
	fs=fopen("part3.don", "w");
	if (fs == NULL){printf("pb ouv fichier part2.don\n");exit(-1);}
	fprintf(fs, "%d\n", nbC);
	//fs2=fopen("partatt.don", "w");
	//if (fs2 == NULL){printf("pb ouv fichier part2.don\n");exit(-1);}

	saveC(lC, fs);
	//saveC(lC2, fs2);
}

void saveC(Listecand lC, FILE *fs)//sauvegarde un candidat
{
	if (lC == NULL)return;
	fprintf(fs, "%d\n", lC->cand.nEtu);
	fprintf(fs, "%s\n", lC->cand.nom);
	fprintf(fs, "%s\n", lC->cand.prenom);
	fprintf(fs, "%.2f\t %.2f\t %.2f\t %.2f\t %.2f\n", lC->cand.moymat, lC->cand.moyfr, lC->cand.moyen, lC->cand.moyspe, lC->cand.noteDoss);
	fprintf(fs, "%d\n", lC->cand.nbchx);
	saveChx(lC->cand.lchx, fs);
	saveC(lC->suivcand, fs);
}


void saveChx(Listechx lCh, FILE *fs)//sauvegarde tous les choix d'un candidat
{
	if (lCh == NULL)return;
	fprintf(fs, "%s\n", lCh->chx.ville);
	fprintf(fs, "%s\n", lCh->chx.dptmt);
	fprintf(fs, "%d\n", lCh->chx.dec);
	fprintf(fs, "%d\n", lCh->chx.valid);
	saveChx(lCh->suivchx, fs);
}


Listechx searchChx(Listechx lchx, char *ville, char *dptmt)
{
	if (lchx == NULL){return lchx;}
	if (strcmp(lchx->chx.ville, ville)==0 && strcmp(lchx->chx.dptmt, dptmt)==0)return lchx;
	return searchChx(lchx->suivchx, ville, dptmt);
}


Listecand AcceptedOrWait(Listecand lC, int *nbC, float noteMin)//créée une liste de candidat en fonction de la note minimum à avoir
{
	Listecand newlC = initC();//la nouvelle liste en question
	Listechx found;//pour la recherche si le candidat a le département informatique de Clermont
	int cpt, i = 0;
	for (cpt = 0; cpt < *nbC; cpt++)//en passant par tous les candidats
	{
		found = searchChx(lC->cand.lchx, "Clermont-Ferrand", "Informatique");
		if (lC->cand.noteDoss >= noteMin && found != NULL)//on check s'ils ont la note d'étude de dossier requise et qu'ils ont une candidature informatique clermont
		{
			newlC = InsertN(newlC, lC->cand);//si oui on les ajoute
			i+=1;//et comme on a une nouvelle liste on les recompte
		}
		lC = lC->suivcand;//on passe au suivant peu importe ce qu'il arrive
	}
	*nbC = i;
	return newlC;
}


void MajDecJury(Listecand lC, int nbC, int nbAcc, int *nbCacc, int *nbCW)
{
	int cpt;
	if (nbC > nbAcc)//si le nombre de candidats est supérieur ou égal au nombre max de personnes mises en "accepté"
	{
		for (cpt = 0; cpt < nbAcc; cpt++)//tous les acceptés
		{
			Listechx iutclinf;
			iutclinf = searchChx(lC->cand.lchx, "Clermont-Ferrand", "Informatique");
			iutclinf->chx.dec = 1;
			lC = lC->suivcand;
		}
		*nbCacc = cpt;
		for (cpt; cpt < nbC; cpt ++)//la liste d'attente
		{
			Listechx iutclinf;
			iutclinf = searchChx(lC->cand.lchx, "Clermont-Ferrand", "Informatique");
			iutclinf->chx.dec = 2;
			lC = lC->suivcand;
		}
		*nbCW = nbC - *nbCacc;
	}
	else
	{
		for (cpt = 0; cpt < nbC; cpt++)//tous acceptés si il y a moins de candidatures que de places
		{
			Listechx iutclinf;
			iutclinf = searchChx(lC->cand.lchx, "Clermont-Ferrand", "Informatique");
			iutclinf->chx.dec = 1;
			lC = lC->suivcand;
		}
		*nbCacc = nbC;
		*nbCW = 0;
	}
}


Listecand searchCand(Listecand lC, char *nom, char *prenom)//cherche un candidat dans une liste en fonction de son nom et prénom dans une liste de candidats
{
	if (lC == NULL)return lC;
	if (strcmp(lC->cand.nom, nom)==0 && strcmp(lC->cand.prenom, prenom)==0)return lC;
	return searchCand(lC, nom, prenom);
}


Listecand MajNote(Listecand lC, char *nom, char *prenom, float newNote)//mise à jour de la note de dossier du candidat corresondant au nom et prenom
{
	Listecand c;
	c = searchCand(lC, nom, prenom);//recherche du candidat
	if (c == NULL)
	{
		printf("Aucun candidat n'a été trouvé...\n");
	}
	else
	{
		c->cand.noteDoss = newNote;
	}
	return lC;
}


Listecand splitAccepted(Listecand origin)//trouve ceux qui ont le département informatique de clermont et 1 en décision et les mets dans une liste des acceptés
{
	Listecand lCAcc = initC();
	while (origin != NULL)
	{
		Listechx iutclinf;
		iutclinf = searchChx(origin->cand.lchx, "Clermont-Ferrand", "Informatique");//cherche le département info clermont pour avoir l'info sur le département
		if (iutclinf->chx.dec == 1 && iutclinf !=NULL)// si il existe (on est jamais trop surs) et que c'est à 1
		{
			lCAcc = Insert(lCAcc, origin->cand);
		}
		origin = origin->suivcand;
	}
	return lCAcc;
}

Listecand splitWait(Listecand origin)//trouve ceux qui devront être en attente et 2 en décision et les mets dans une liste d'attente
{
	Listecand lCW = initC();
	Listechx iutclinf;
	iutclinf = searchChx(origin->cand.lchx, "Clermont-Ferrand", "Informatique");
	while (iutclinf->chx.dec == 1 && iutclinf != NULL)
	{
		origin = origin->suivcand;
		if (origin == NULL)break;//on évite le tour de search inutile
		iutclinf = searchChx(origin->cand.lchx, "Clermont-Ferrand", "Informatique");//tant qu'ils sont acceptés, on relis le candidat
	}
	while (iutclinf->chx.dec == 2 && iutclinf != NULL)//si en liste d'attente
	{
		lCW = Insert(lCW, origin->cand);//insertion dans la liste pour liste d'attente
		origin = origin->suivcand;
		if (origin == NULL)break;//on évite le tour de search inutile
		iutclinf = searchChx(origin->cand.lchx, "Clermont-Ferrand", "Informatique");
	}
	return lCW;
}


void RespAdmin(void)//fonction dédiée au responsable d'admission
{
		int nbC = 0, nbP;
		float noteMin, note;
		char nom[26], prenom[26];
		Listecand lC = initC();
		Listecand lCAcc = initC(), lCW = initC();//ce qui servira pour les 2 listes, l'une des acceptés et l'autre des attentes
		char rep;

		FILE *fe;
		fe=fopen("part3.don", "r");//ouverture fichier
		if (fe == NULL){printf("pb ouv file"); return;}
		lC = Chargementlistecandidat(fe, lC, &nbC);
		fclose(fe);

		printf("*********************************************************************\n");
		printf("*----------------Modification note dossier (O/N)--------------------*\n");
		printf("*--ATTENTION IL NE SERA POSSIBLE DE MODIFIER CES NOTES QU'UNE FOIS--*\n");
		printf("*********************************************************************\n");
		scanf("%c", &rep);//on récupère la réponse de la volonté du responsable des adimissions
		while (rep == 'O')
		{
			printf("Quel est le nom du candidat ?\nNom : \t");
			scanf("%s%*c", nom);
			printf("Quel est le prénom du candidat ?\nPrénom : \t");
			fgets(prenom, 26, stdin);
			prenom[strlen(prenom)-1]='\0';
			printf("Quelle sera la nouvelle note ?\nNote : \t");
			scanf("%f%*c", &note);
			lC = MajNote(lC, nom, prenom, note);
			printf("Souhaitez-vous changer/renseigner une autre note ?\n");
			scanf("%c", &rep);
		}

		printf("Quelle est la note minimale d'admission souhaitée ?\nNote :\t");
		scanf("%f", &noteMin);//on récupère le note minimale requise pour être accepté ou en liste d'attente

		lC = AcceptedOrWait(lC, &nbC, noteMin);//on créé la nouvelle liste des candidats sélectionnés par note

		printf("Quel est le nombre de places disponibles ?\nNombre de places :\t");
		scanf("%d", &nbP);//on récupère le nombre qu'il peut y avoir d'acceptés pour savoir qui sera accepté et qui sera en attente
		int nbCacc, nbCW;
		MajDecJury(lC, nbC, nbP, &nbCacc, &nbCW);//fonction qui maj dec du jury
		lCAcc = splitAccepted(lC);//la liste des acceptés

		lCW = splitWait(lC);//la liste d'attente


		FILE *fA;
		FILE *fW;

		fA = fopen("partAcc.don", "w");//ouverture du fichier de sauvegarde des acceptés
		if (fA == NULL){printf("pb ouv fichier acc\n"); exit(-1);}
		fprintf(fA, "%d\n", nbCacc);

		fW = fopen("partWait.don", "w");//ouverture du fichier de sauvegarde de l'attente
		if (fW == NULL){printf("pb ouv fichier wait\n"); exit(-1);}
		fprintf(fW, "%d\n", nbCW);
		
		saveC(lCAcc, fA);//sauvegarde des acceptés
		saveC(lCW, fW);//sauvegarde de l'attente

		fclose(fA);
		fclose(fW);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//histoire d'avoir un AFFICHAGE

void AffC(Candidat c)
{
	printf("%d\n", c.nEtu);
	printf("%s\n%s\n", c.nom, c.prenom);
	printf("%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", c.moymat,c.moyfr, c.moyen, c.moyspe, c.noteDoss);
	printf("%d\n", c.nbchx);
}

void AffCh(Choix c)
{
	printf("%s \n", c.ville);
	printf("%s \n", c.dptmt);
	printf("%d \n", c.dec);
	printf("%d \n", c.valid);
}


void AffListeCandidat (Listecand l){

	if (l == NULL) return;
	AffC(l->cand);
	AffListChoix(l->cand.lchx);
	AffListeCandidat(l->suivcand);
}


void AffListChoix(Listechx l)
{
	if (l == NULL) return;
	AffCh(l->chx);
	AffListChoix(l->suivchx);
}


/*
printf("........................................................::.::::::::::::::::::::::::::::::::::::::::::::::::::::::^^^^^^^^^^^^^^^^^^^^~~~!!!!!!!!!!!!!!!!!77777777777777777777777777777777777777777777777777777777777777::::...:^::..:.::..:...:..:..:..:::\n");
printf(":::::::::::::::::::::::::::::::::::::::::::::::::::::::!77777777777777777777777777777777777777777777777777777777777777777777777777777Y555555555555555555555555555555555555555YYYY555Y5YYYYYYYYYYYYYYYYYY55YYYYYYYYYY5YY.......:^~:::^::^..^:.:^..^:.^:^^^~\n");
printf(".......................................................!?77!7777777777777777777777777777777777777777777777777777777777777777777777777Y5YY5YYYYYYY5YYYYYYYYYYYYYYYYYYYYY5YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY.........~~^^^.^:.^:.^:.^:.^::::~^:\n");
printf("......:.:....::.:.....:.....:.:::::::::::::::::::::::::!777!7777!77!!7!7777777777777777!777!777777777!777777777!77!777!!777!7777777!7YY55YYYYYYYYYYYYYYYYYYYYY55YYYYYYY5YYYYY5555555YYYYYYYYYYYYYYYYYYYYYYYY5555555YYJJ:......:..!!^^^:.::.:::::::.^:^^!:.\n");
printf("!!!!!~.:^!?7~.::.^!??~.....~555PPP5P5PPPP5P5PPPPPPPPPPG?~!77!!!77!!?77?7777?J77J????777J?77J??JJ77!??????7JJ777J?7JJ??JJ77!?!?77?7?!7Y5YYYPBGGGGBGBBBBBBBGGGGBBBYYYYYYYYY5PGBBBBBBBBBBGPYYYYYYYYYYYYYYYYYPGGBBBBBBBBGP5^.........:7~:^!!~^:!!^::~!!::^~~.:\n");
printf("Y7777~.!Y7!JY~..~Y?!7Y7:.:.~GGGGGGGGBBYPGJ5G5PBBGGGGGGB?!!JY?^?PPJ5Y!YP?!7!GY75J?YGG?7?P57JG?PPG77!P5?PG5!YP777GJ7GP?Y5Y7!7J7?77?7777JYYYYPBBBBBBBB######BBB###BYYYYYYYYGBBBBBBBBBBBB#BBBPYYYYYYYYYYYJ5PB#BBBBBGGBBBBBBG?:....:...7!^~P!7~J?^Y775^J?^^!^.:\n");
printf("J.....^Y?:.:?Y^.JJ^..!5~...~PGGGGGGP5P5PGPGG5P5PBGGGGGB?~!!!!~!JJ77J7?J?777JJ7?J??JJ777JJ?JJ?J??7?7?J?J?J???777??7J7J??777777!7~~~~!!JYY5YPBBB#B5PPPPPPP5PPPPPP5YYYYYJ5BB#B#BP5YYY55GBBB#BGYJYYYYYYYY5BBBB#BPYYYYY5GBBBBB5^.......!!^~!~Y?5!:Y?JJ.?Y^~!^..\n");
printf("J!!~:.~5!:..7Y~:Y?:..^Y7:..~PGGGGGBPYPGGGGGBBB5YGGGBBGGJ~!!!!!!!!!!!!7!7777777777777?77!!7777?777?77?77777777777777777777?!7~?5PJ?!~:?YYYYGBBB#GYYYYYYYYYYYYYYYYYY5YYYBBBBBG5YYYYYYYJ5BBBB#GYYYYYYYY5B#BBBGYJJYJJYYJ5BBBG#P: :.:..7~~~77?!~?7?^^?7?~^^!~..\n");
printf("!77YJ^~5!:..!Y!^5?:..^Y7:..~PGGGGGGJ5BGGBGGBGBPJ5BGGGGGJ~!!!!!!!!!!!!77777777777??777777???7?J77??77?777777777777777777777!~?YJB###BGGP5YJGBBB#GYYYYYYYYYYYYYYYYYYYJYBBB#BGYJYYYYYYYYJP#BBB#GYYYJJYYB#BBBBYJYYYYYJJYJ5BBBBBY.....^7^^^:^^:^^^::::^::~^~!:.\n");
printf("...^JY~Y!...7Y~^Y?:..^Y7:..~PGGGGGGGJ5GGBGGBGGYYBBGBGGBJ~!!!~!!!!!!!!!7777777777?7777???JJ77!77?7777~777777777777777!!77!777YYJYP5YYY5BPJJP#BB#GJYYYYJJYYYYYYYYYYYYJPBBBB#5YYYYYYYYYYYJGBBBB#YYYYJJP#BBBBPJYYJYYYJYJYJGBBBBB~....!~^^:^::::::::^^::^:^^~^.\n");
printf("...:YJ~Y?:.:7Y^:?Y^..~5!...^PBGGGBGGPGJ5B5PGY5GPGGGBGGBJ~!!!!!!!!!!!!!7777777777777????JJ5JJJJ?7?J77^!?77777777777777!?Y5555YJ??7~:^?PB5JJPBBBBGJYYYYYYYYJJJJYYYYYYJGBBBBBYYYYYYYYYYYYJPBBBB#5JYYJJGBBBBBYYJYJJYJYJYYJ5BBBBBY...~!~^~.:^:^::::::::^::^:^~^\n");
printf("~~!JJ~.!Y?~?J!:.^JJ!7Y7:...~PBBBBBGBBBGGGYPBGBBBBBBBBGBJ~!~~~!!!!!!!!!!77777??777???7??7!?77?7?7??!!!~7777777777777777777?Y55YJ?!?PB##B5YJGBBBBBBBBBBBBBGGPYJJYJYJJ5BBBBBGYYYYJYYYJJYYJYBBBBBGJYYJJBBBBBBJYJYYJJJJJJJJYBBBGBP:.~!:^.^:.^^.:^..^:.^^.^::^^~\n");
printf("???!:...^777^..:.:!7!~:....^J5YYYYYYYYYYYYYYYYYJJJJJJJJ?!7!!!!!!!7777777777777777??77777777!!!~!!!^:^~~~~!!!!77777777777!755YJ??7Y#BBB#YJJG##BBBBBBBBBBBBBBBGPYJJJJ5BBBBBGJYYJJJJJJJYJJJBBBBBBJYJJJ#BBBBGJJYJJJJJJJJJJJBBGGBG^~~!^^:.::.:^:.::..^:.:~.:^^~\n");
printf(".::....................................................~7?77777777777777777777777777777777777!~^^^^^^^^^^^~!7777777777777JY?!JY?!~PBBB#5JJPP55YYYJJYYY5GBBBBBBB5JJJ5BBBBBGJJYJJJJJJJJJJJBBBBBBJYJJJBBBBBBJJYJJJJJJJJJJJBGGBBG~..^~!~~^^^^:^^::^:^:::^~^!~:\n");
printf(":...:...::....................::.......................~7?77777777777777777777777777777777??7777!^^^:^^^!?77777777777777!77!!J5BPJ?BBB#5JJJJJJJJJJJJJJJJJ5BBBBBB5J?YBBBBBGJJJJJJJJJJJJJJBBBBBGJJJJJBBBBBBJJJJJJJJJJJJJJBGGGGP^....!!~^~^^^^^^^^^^^^^~^!~:.\n");
printf(":..:::.................................................~777?77777777777777777777777777777777?7?7~^:^~^::~77777??777777777777!JYBBB#BGGB5JJJ?JJJJJJJJJJJJJJYBBBBBBYJJBBBBBBJJJJJJJJJJJJJJBBBGBP?JJJ?GBBBBBJJJJJJJJJJJJJJGGGGG5..:^~~^^^^^::::::::^:^::^^^^^\n");
printf(".......................................................~7777777777777777777777777?7777?77777?7?!:^~!77~^^!?77?77777777!7777!!JYGBBB#P?JJJJJJJJJJJJJJJJJJJ??PBBBBBY??PBBBBB5?JJJJJJJJJJ75BBBG#YJJ???5BBGBGP?JJJJJ??JJ??5GGGGG7.^^7^~^:^.:^:::::::::::^::^:!\n");
printf(".......................................................~777777777777777777777777?77?77777777777!!77??77?!!77777777777?????77Y5JP#B#BBY?J??J?JJJJJJ?????????GBGBBBJJ?JBBBGBP???????????JBBGBBGJJJ???JGBGGBGJ?J???J????JPBGGGB!....^!!~~^^^^^^^^^^^:^:^^~~:.\n");
printf(".......................................................~777777777777777777777777?~~??777777777?77777777777777777777!7?JJJJ??5YJ5#BBBBBJ???????????????????5BGBBBP????5BBGGGP??????????GBGGGGJ?J?J???JGBGGGGJ7????????PGGPGGY!....:~!~^^^^^^^^^:^^:^:^~~~^.\n");
printf(".....................................................:.~7777777777777777777777??!^^!777777777777777777777777777777777?J??J??YYYYBBB#B#P???YY???????????J5GGBGGG5?J????5BGGGBPJ??????YPGGGGGY?????J???JGBGGBGYJ?????YPGGGGG!~7.:^~~~^:^::::::::::::::^::^^^\n");
printf(".....................................................:.~7777777777777777777!!777~^^^!77777777777777777777777777777777?J?????YYYYP55PGGBY7?PBGPP555555PGGBBGGGPY?7?????7JPGGGGGGP55PGBGGBBPJ???J???????JPGGGGGPPPPPPGGGGG5!:~7.^~!.^^.^:.:::.::.::^:.:::::^\n");
printf(".....................................................:.^77??77777777777777!~^:^^^^^:^^^^~!7777777777777777777??777777JJJJJJJ5YYJGBGGGPP5J?PGBBGGGGGGBBGGGP5YY5Y5PJ7?J55Y5GGGGGGGGGGGGGPP5JJYYYJJJJYJJJ??YPYPGGGGGGGPPGJ7!~~77...~~^::^::^.:^:.^:.^:.:^:^^~\n");
printf(".......................................................^7777777777777777?77?7!^^^^^^^~!!777777777777777777777??777777???????5YY?PBBBBBB#GY??JJY55PP55YJYJ777PGGG#57??JYPPPPGGGYPP5YYY5Y??JJJYJ????JJ7777?J~7?JJ5J??7!Y!::::~7...:!^:^::^.:^.:^::^.:^.^:^!^\n");
printf(".......................................................^777777777777777777?7??~^^^^:^~!777777777777777777777777777777Y5555YPPPY?5BBBBBBBB#Y~!!!7777????J?777YGGG#P7??7YGPGGGGP5GPPPP5PP5PPPPPPPP5PPPPPPPPP5P555P5P555PPP55P57....^!~^:^:^:::::^:.^::::^~~.\n");
printf(".......................................................^77777777777777777777?!^^~!!~^^~77777?7777777777777777?7777777YPPPP5PPP5JYBBBBBBBB#5!!7777777777?J???JP55BG???JPGP5YY55GP5Y55Y55YY5YY5YYYYY55YYYY55JYYJY5YYYJ?5?!!!!77...:.~!:^.:::^::::^^:^:^^^!:.\n");
printf(".......................................................^777777777777777777?77~!7777?7!~!77777777777777777777777777777J?????J5Y5?JBBBBBBBBBPJ!777?77?7???????YGPPBB?7JYYY5J????J5J????YY????JYJ????JJ????JY?????JJ??!:J~....^?.:...^!^^^::^:::::::::^^:~~..\n");
printf("....................::.................................^77777777777777777?777??7?77777777777?7777777777777777?7777777JJJJJJJ5Y5J?GBBBBBBGBGJY!77777?7??7????JPPPGBY????J5J??J??YJ?J??YY?J??JYJ????JJ????JY?????YJ??~:J!....^7...:.:!^^^::::^!7^^::::^^~~.:\n");
printf(".................::::::................................^777777777777777777777??7777777?777777777777777777777777777777J?????JYJ5J7PBBGBGBBB#JB?7?7?7????7J???JPPPP#57?JJJ5J????7JYJJ?JY5JJ?JJYJ????JJ????JY?????YJ??!^J!:.:.^?...:.^!^^^:~77JJJJ7!7:::^^!::\n");
printf(".......:::::::.:.......................................^77777777777777777?777?7777777777777777777777777777777??777777YYJJJJJ5Y5J75GGBBGBBG#JGG77?7?????7JJ???5GPP#P?JJJY55YYYYYYYYYYY55YYYYYYYYJJJYYJJJJYYJJJJJYYJ?7!Y?~~~~~^....:!~^~~??JY7JJ?JJ7J7~^^~~.\n");
printf("......::::::::::.......................................^777777777777777777?~77777777777777777777777777777777777777777J?77?7?Y?5J7YGGBGGGBGBYPBP7???????7JJ???5PPP#PJJJJJ5YJJJJJJJ?JJJY5JJJJYY?77?7JJ?77??Y7?7?7Y777~^Y7:::...:...~!7!?J7J??J??YJ?JJ?J!7!~:\n");
printf(".......:::::.:::.......................................^7777777777777777777^!7??7777777777777????77777777777777777777YP55555PP5J??GGGGGGGGG5PGG57????????????YPPPBGJJJJYPPPPP55GP5P5PGGPPPP5PP5555PP5PPPPG5555PP555P5Y7^.......:^!?!!!?7?Y7!77!?YJ?~!!!!7!\n");
printf("::.....................................................^777777777777777??7~^^7??7777777777777???7777777777777??777777YP5P55PP55J?7GGGGGGGGGP5GGGJ7???????????Y555GGJJJJYP5Y55555YJYYY5PY5555PYJJJJ55JYYYYPJJJJJ5YJJ7~:.....::~?7!?J~77YJ??^?JY?^YJ~?J??^?!\n");
printf("::.....................................................:77777777777!~~!!~~^^^~~!~~~!?7777777777777777777777777?777777J7!77!7Y?5J?7PPPPPGGPPPJGPGPJ??????????JY555GBYJJJY5?~!!!~??~~!~?J!7??JY?!~!!??~!!~7Y~~!!!Y7^:........~J5J!!JJ!??!~J!:5?7J^?!^??YJ^7!\n");
printf(".......................................................:777777777777!~^^^^^^^^^^^!77777777777777777777777777777777777J77!7!7Y7PJ?!J5Y5555PPPJPPPP5J??????J??JJ55YP#5JJJJ5J??!!~??~!!!?YJJJJJY7!~~~??~~~~!J~~~~!Y7~~^........:^77!7??Y?Y~7Y^7?Y7^YJ~7Y7?^?7\n");
printf(".....................................................:.:7777777777??777!^:^^^^^~7??77?7777777777777777777777777777777J7!7!~!Y7PJ?!JP55555Y5575YYY5J??????Y??JJY555#PJJJYPYJJJ!!??!!!!J5JJJJJY?!!!!??~!!~7J!~~!~Y7~~^:?!........:^!7?!!7!?Y?7777??YJ!7!!77!\n");
printf(":::...........:......................................:.:7777777777777?7!^^^~^^^~7?7777777777777777777777777777777?777JJ7777?YJ5J?7?P555P55PPJ55PP55Y??JJJJ7?JJJYYYBP?JJJP5JYJ77JY??77J5YJYJY5J7777J?7777?Y!!7!7Y77!!!J7~!^.......~~!!??7Y??Y??JJ7JJ??~!~!:\n");
printf("::.....::::::........................................:.:7777777777777?7~^!777!~^!7?77?777777777777777777777777777?777J7~~~~!Y7PJ??7P55555555YY555555J????Y?JJJYYJYGGJYYY5Y??!77JJJJ?7?Y?JJ??Y?~~~~??~~~~!J~~~~~Y7^^^^?!::::^~....:!~^:^??JY??J??Y7?!^:^~~.\n");
printf(":::....::::::..........................................:777?77777777777!77777777!7?777777777777777777777777777777777!JYJJJJYPY5J?7!5PP555PP55JP55Y555JJYJY?JJJYYYYG#YJYYPGP5555GP55P5PP555PPP5Y55555Y5555PY5YY5P55555P5Y55P5J..:..^7^^^^^!~?J?J7~~:^^^~!:.\n");
printf("::::::::::::::::.....................................:.:7777777777777777777?77777777777777777777777777777777777777?7!JGPPPGPPPPJ?7!YP55555555?P5555555PPPPYY5P5JJJP#5YYYPPPPPPPPPPPPP5PPPPPPPP555555555555555YY55YJYJ5YJYYJJ?.....^7~^^^^:::^7^:::::^^~~::\n");
printf("::::::::::::::::.....................................:.:7777777777777777777777777777777777777777777777777777777777777?J!!!!7Y?PJ?7!?555YYYYYY7YYYYYYYY5555YJPP5JJJ5#PYYYPJJJJJJYYJJJJJYJJJJJY?~~~~??~~^^!J~~~^~Y7^.::?!.::.:?.....^!~^^::^:^:.^^:^:^:^^!:.\n");
printf(":::::::::::::........................................:.:777777777777777777777777777777777777777777777777777777777777??7^^^^~J!PJ??!?PPPPPP555J5P55555Y55555J5YYJJJYBPYYYPJ?YJ??JJJJ??JYJ?JJJY?^^^:??::::~Y^:^:^Y!:.:^?!....:?.:.:.^!:^::^.^::^:::::::^^!^.\n");
printf("::..........:....:::................................:.::777?777777777777777777777777777777777777777777777777777777777?7::::^Y!PJ?7!!5Y555Y5YYJY5Y55YYY5PPPPYGPPYJJYBGJYY5J?G57?JJ?????Y????JY7::::?7::::~J^:::^Y!: .:?!....:?.....!~^:^::^:^:.^:.^::^:^^~:\n");
printf("::.....:::::::.:....:::::::::::::.......::::::....:::.::777?7777777777777777777777^7??77777777777777?7777??7777777777J7^^^^~Y7PJJ?!!?????????Y7??JPYJ??BGYYJY55J??JGBYYYPY~?GJ?YYJ?JJJYJJJJJY?!!~~??~~~~!Y!~~~!Y7~^~~J7~~~~~J.::.~~^^::^.:^.:^:.^::^:^^^~!\n");
printf("::.....:::::::::....::::::::::::::......:::::::.:::::.::7777777777777777777?????7~^~7??777777777777777??7777777777777J?^^^:~J!PYJ77!?????????Y7???PYJ?7GGPYJYYYJ???G#555G?^^YGJYY?????Y?????YJ!!!!??!!!~!J!~~^~J7^^^:?!:::.:J...^!^~:^::^:.^:.::.:^::^:^:~\n");
printf("::::::..::..::......:::........:::::::...............:.:7?77777777777777777!!77!!^^^~77!!!7777?7777?77777777777777??7??!~~~7Y?PY??!~777777777?77775YY?!5GG5JJYYJ??JJJ7???^^:~?7??7?PP55YYJYY55555YY5YYYYJ5YJYJJ5YJJJJ5Y?JJ?JJ::~~^^^:^:::::::::::::::::^:^\n");
printf("::::::::..::..::....:::::......:::::::..:::::::......:.:7?77777777777777777!^^^^^^^^^^^^^~!77777777777777777777777?7!JPGGPPPGPPY??!~!7!!!!!!!7?!!!J5J?!JGGP5JYYY77!YY!^:^^^^^^:~7YPP5PP55PP5P5PPP5PP555555555Y5P5555555YYYYYJ::~~!~~:^^:^::::::::^::^^:^^~\n");
printf("::::::::::::::::::..:::::::....:::::::..:::::::......:.:7?777777777777777???77~^^~^^^^~7??777777??7777777?77777777??7?YYYYYJYYPY?7!~~~~~~~~~^!7~~^75J?!?BPGG?YYY77!JBPJ!^^^^^:?GJ777!7J7!7!7JJJJJJYJ?7777J7!7!!J?~~~^?7:.:.:?^~^^^!7!!~~~~~~~^^^^~^^~~~!^:\n");
printf("::::::::::::::::::::....................::::::::::::::.:7?7777777777777777????7^:^^^^^!?7777777777777777??7777777?7?77YJYJYJJYPY?77~^~~^^~~~^!7^^^!YJ?7?P5557YYY777?BGJ^:^!!^^~GY!!!!!J7!!!7JYJJJJYJ7777!?7!!!!J?~~~^7!..::.?^^~^^!7!!~~~~~~^^^^^^^^^~~!~:\n");
printf(":::::::::::::::::::::::::::::::::.....:::::::::::::::..:7?777777777777777?7?7!~^~!7!~^^!??777777777777777?7777777?7?7?YYYYYYJYPY??7~~YP555P555JY555YJ?775PP5?5YY?!!!BB!~?Y?!~~^!G7~!!!J7!!!7JYYYYJYJ??777J?!7!!J7~^^:77:...:?^~!!7~!~~~~~^^^^^^::^:::^:^^~\n");
printf("::::::::::::::::::::::::::::::::::..:::::::::::::::::..:!?777777777777777???7!!7?77??7!!7??7777777777777??7777????7?7?YYYYYYJYPY??7~~5GPPPGPPGY5GPG5Y?7~YGPPYP55?~~^PGY5P5!!77!?P5!!77J7!!!7YYYYYY5YJJ???YJ7?77J?!!7!??!~~~!J^^~~!!!~~~~~^^^^^^^^^^^^^^~~^\n");
printf("::::::::::::::::::::::..::::::::..:::::::::::::::::.::.:!7777777777??7777??77?????????777??777??7777??777?????77777?7?5YY5YYY5PY??7~^JPPPGGPPGYPGPG5YJ7~JGPP5YP5?^~:5BPY5Y!!!!~??~!~~!J7!~~!J5555YYJ????7JJ7777J?!~~^7?:::::?^~^^^~7!~~~~~~~~~~~~^~^~~!7^.\n");
printf(":::::::::::::::::::.:....:::::::..::::::::::::::::..::.:!7777777777?777777777777???7777777777?777?77J??777777777777?7?55Y555Y5PY??7!^7PP5P55PPYYPPP55Y555P555YP5J^^:JBG555J???JYY?JJJJ5J?JJJ555555PYYYYYJYYYYYJ5YJYJ?YYJ?JJJJ^~~~~7!~!~~~~^~~^^^^^^^^~^~!~\n");
printf("...........:::........:::.....:::...::::........:.......!777!!!!777777777!!777??77777777777777777???J???777777???77?7?PPGPPPGPPY??7~^~YJYYJY55J!777??7!~~!55?5PPP5555BGY5PPPPPPPP5PPP5PPP5PPPPPPPPPPPPP5PPPP555PP55P555YY5YYJ^~7!7~!^~!^^~^:^~^::^:.^^:~^^\n");
printf("^JJJJJJJJJ^:..!JY55Y!:.:.:.:7Y5P5J!:.:..:!!!!!~.:^!~..:7?777JJYYJ777777JYYY?777777777777777777???JJ?JJJJJJJ???7777777?55P555Y55J??7!^^GBBBBBB#G!5GGGBJ:.^^5G?PGGGPPPP#G5PJ~^^^^7?^^^^~J~^^:~J555555J????7J?!77!??~~~^!?:::::?^^~77~~^!^^~^:^~.^^::^:.~:^^^\n");
printf("~BP777!7!!^.^YBP7~7YBP~...~PB5!~!5B5^.:.~#P7!7~..?#5..!#G77?#GJJGBJ7!JBPJJYBP?7???7777777777777??JJ?JJJJ????7777?77777Y55555JPYYJ?7!^:GBBBB#BBB!YBBBB5^^^~YG?5GGPPPPPBG55J:::::~?:.:.^J^:..^?555555J7?777J?!7!!?7~~^:!?:.:..?~^~~7!!^~^~~^^^^^::^::^::^^~!\n");
printf("~B5:.. ..:::JGG^.:::5BP^:^PB5:...:GBY:..~#PJ?J!..7#Y..~BP!77BGY5B577!G#?~7~5#Y7???777777777777?77?J???JJ?77777???77777Y555555YY5J7!~^.5##B#BBBB7?BBBGG^~~7JGJYPGPPPPPBB5YJ:....~7....:J^...:?555555J7777!??!!!~77~~^:!?.....?^~^^~7!!~~~^^^^^^:::^:^^:^^!^\n");
printf("~#PJ??7!:..^PB?.::..!BB7:~BG7.::..JBP~:.^#P~~~^..~BP~^J#5!7?#G7JBG77!Y#P?JJBGJ77777777777777777??JJ?7???J?7777777777775555555YYYYJ?77!PBB##BBB#?7BBBGB~!!7?GYJPGPPPPPBB5YY~^^^^!J^^::~J!^^^~J555555Y????7JJ7?77J?!!!~7J~~~~~J~~^^^!7!~~^~~^^^^::::~:^^^~~.\n");
printf("^Y???J5GP?.^PB7.::..~BB?.!BG!.:...7BG!..^GP555?.:^?5YJYY?77?P5??Y5J777J5PPPY77777777777777777777??7777777?7777777???JJ5P5YYJYYYYYYYJ7~?#B#BBBBBJ!BBBGB!7777GPJPGPPPP5GBPYY^::::!J^:::^J~:::^JPP5PPPJ777?7JJ!!!!??~!~^~J^::::J^^^~^~7~~~Y!!~7J7?^~J7J~^^7^.\n");
printf(":...:..!GB?:PBJ..:..!BB7.~BB7.....?BP~.::??!~~^..^7?!:7J?7!??????J?77?JJ7?J?????777777777777777??????JYYYYY555PPPPGGGPYPPP5YYYYYYY?777!#BB#BBB#Y~GBBBB?!???YPJPGPPP5YP#GY57!!!!?Y!!7!7Y7!!!7YPP5PPP5JJJJJYYJJJ?YY?????577???J~~~~:^7!~!J!J75!:Y?JJ.?5^^7^.\n");
printf(": .....^GBJ.JBG^....YBP~::5BP:...:GBP:..^J?7!!~::::!?J?~!?7?J??J???7JJ??77?JJ??77??????JJJYYYY55555PPP55PP5PPP5555555PGGPP5YY55YYYYYY57BB#BBBB#5^PBBBG?~??J??JYYYYYYYP#G5PGPPPPPPPP5PPPPPPGPPP5555PPPPPP555P555PPPPPPP5555Y5J^^^^^^7!~~!~Y??J~Y~~5~J?^~!^.\n");
printf("^7~^^~?PG5^:^YBG7~~YBG!...^5BP!~!PB5^.:.^JJ!^^^..:..7J!.!77?J???7777?JJ?7??JJ?7????????????????77777777777777777777777JJ?Y5J??PPP55PPP?5GGGPP55J!JYYYYYYJJJJYYYPGB####BBY7??????????????????????7?????????????????????77:::::::.::^!^^~~!^^^^!^:^^~~:^^!~.\n");
printf("^P55P55J!:...:!J55557^.:..::!Y555Y!::::.:77777!:.:..~7^.!77?J?777777?JJ????J???777777777777777?77777777777777777777777777?7777PPPPPPPPY?JYY55555PPPPPPGPPJYYYYYY55PPPPPPY?J??JJ????????????????????????JJJ????????????J?.::...::.:~~~:^:.^::^::^::::^:^^~^\n");
printf(":.::::::.::::::.::::..:::::..:::...:::::::.:::::::::..:.!?7??77777777??77777????77777777777777?777777777777777777777??!777777777777?77JJJJJJJJJJJJJJJJJJJJYJJJJJJJJJJJ?JJJYJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ::::...:.^!^:^:^:.^::^^.::.^::^:^!\n");
printf(":::::::::::::::::::::::::::::::::::::::::::::::::::::.::!?777777777777777???????77777777777777??77777777777???????77777777777777777777Y5YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY.::.....:~^^:^:.::.:^.:^:.^:.^^::^\n");
printf(":::::::::::::::::::::::::::::.....::..:........::....::.~77!777777777777777777777777777777777777777777777777777777777777!!!!!!77777!!!J5YYYYYYYYYYYJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ???????????????7.:::::::^^:::::::::::::::.::.:::::\n");
printf("::.........::.......:::::::::::..:::::::::::::::::::::::^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^::::::::::::::::::::::::::::::::::::::::^::^^:::^:::..............:::::::::::::....................................     ....... ..  . .  .  .......  .  ..\n");

Voici un petit billet pour vous madame
*/

/*
printf("
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒░░░░░░░░░░░░░░░░░
▒▒▒░▒▒▒░░▒▒░░░▒▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▒▒▒▒▒▒▒▒▒▒▒▓▒▒▒▒▒▒▒▓▓▓██████████▓▓▓▓▓▓█████▓▓▓▓▓▓▓▓▓▓▓████▓▓░░░░░▒░▒░░▒░▒▒░▒░
▒░░▒▒░▒░▒░░▓░░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▓▒▒▒▒▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓▓▓▓▓▓▓▓▓███▓▓▓███▓▓▓▓▓▓███▓▓▓▓██▓░░░░▒░▒▒▒░▒▒▒▒▒░
▒▒▒▒▒░▒▒▓░░▓░░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓█▓▓▓▓▓██▓▓▓▓▓▓▓▓▓▓▓██▓▓▓▓▓▓▓██▓▓▓▓███▓▓▓▓▓▓██▓░░░▒░░░░░░░░░▒░
░░▒▒▒░▒▒▒░░▓░░▓▓▓▓▓▓▓▓▓▓▓▓▓▓░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▒▒▒▒▒░▒▒▒▒▒▒▒▒▒▓▓▓▒▒░▒▓▓▓██▓▓▓▓▓▓▓▓▓▓▓██▓▓▓▓▓▓▓███▓▓▓███▓▓▓▓▓▓▓██▒░░░░░░░░░░░░░░
▒▒▒░▒▒▒░░▒▒▒░░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░▒▒▒▒▒▒▒▒▒▒▓▓▒▒███▓▓████████▓▓▓▓███▓▓▓▓▓▓▓███▓▓▓██▓▓▓▓▓▓▓▓██▓░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░░░░▒▒▒▒▒▒▒▒▒▒▓▒▓██▓▓▓▓▓▓▓▓▓███▓▓███▓▓▓▓▓▓▓███▓▓▓██▓▓▓▓▓▓▓▓██▓░░▒░░░░░░░░░▒░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░▒░░▒▒▒▒▒▒▒▒▒▒▓██▓▓▓▓▓▓▓▓▓▓▓▓██▓▓▓██▓▓▓▓▓▓▒███▒▓▓██▓▒▓▓▓▓▓▓██▒░░▒░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓███▓▒▓▒▒▒▒▒▒▒▓██▓▒▓██▓▒▒▒▒▒▓██▓▓▒▒▓██▓▒▒▒▒▒▓█▓░░░▒░░░░░░░░░▒░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓███▓▒▓▓▒▒▒▒▓▓██▓▒▒▒▓██▓▓▒▓▓██▓▒▒▒▒▒▓██▓▓▓▓▓█▓▒░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▒▒▓▓▓▓█▓▓▓██████▓▓▓▓▓▒▒▓▓██████▓▓▓▓▓▒▓▒▒▓▓▓▓▓▓▓▓▒░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓████▓▒▒▒▒▒▒▒▒▓▓█▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒░░▒░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓████▓▒▒▒▒▒▒▒▒▓▓█▒▒▓▓▒▓▓▓▒▓▓▓▒▓▒▒▓▒▒▓▒▒▒▒▒░▒░░▒░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▒▒▓▓▓████▓▓▒▒▒▒▒▒▒▓▓█▓▒▒▓▒▒▒▓▒▒▓▒▒▓▒▒▓▒▒▓▒▒▒▓▒░▒░░▒░░░░░░▒▒▒▒░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▒▒▓▓▒████▓█▓▒▒▒▒▒▒▒▓█▓▒▓▓▓▓▓▓▓▓▓▓▓▓▒▒▓▒▒▓▒▒▒▒▒▒▒░░░░░▒▒▒▒▒▒▓▒▒▒▒░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▒████▓▓█▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒░░░░▒▒▒▒▒▒▒▒▓▒▒▒▒
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒░░░░▒░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▒▓▓▓▓▓▓▓▓▒▒▒▒▒▒▓▓█▓▓▓▒▒▒▒▒▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▒▓▓▓▓▓▓▓▓▓▒▒▒▒▒▓▓█▓▓▓▓▓▒▓▒▒▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒░░░░░▒▒▒▒▒▒▒▒▒▒▒▒
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▒▓▓▓▓▓▓▓▓▓▓▒▒▓▓▓▓█▓▓▓▓▒▓▓▓▒▓▓▓▓▒▒▒▒▒▓▒▒▒▒▒▒▒▒▒░░░▒░▒▒▒▒▒▒▒░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▓▓▒▒▒░░░░░░░▒░░░░▒░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░▒▓▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▓▒▒▓▒▒▓░░▒░░▒░░▒▒░░▒░░▒░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░▒▓▒▒▒▓▓▓▓▒▓▓▓▓▓▓▓▓▒▓▓▓▓▒▓▓▓▓▒▓▓▒▓▒░▒░░▒▒░▒▒░░▒░░▒░░▒░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░▒▓▒▒▒▒▒▒▒▒▒▓▓▒█▓▓▓▒▒▓▓▒░▒▒▒▓▓▓▓▓▓▒▒▓▒▒▓▒▒▒▒▒▒▒▒▒▒░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▒▒▒▒▒▒░▒▒▓▓▒█▓▓▓▒▒▓▒▒▒▒▒▒▓▒▓▒▒▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒░░▒▒░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒░▒░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▒▒░▒▒▒▒▒▒▒▒▒▓▓▒▓▒▒▓▒▒▒▒▒▓▒▒▒▒▒▓▓▓▓▒▒▒▒▒▒▒░░▒░░▒░▒▒▒░▒░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▒▒░▓▓▓▓▓▓▓▓▒▓▓▓▓▒▒▓▓▓▒▒▒▓▒▒▓▒▒▓▓▓▓▒▒▒▒▒▒▒▒▒▒░░▒░▒▒▒░▒░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▒▒░▓▓▓▓▓▓▓▓▒▓▓▓▓▒▒▓▓▓▓▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▓▒▒▒░▒▒▒▒░░░░░░░░░
▒▒▒▒▒░░▒▒▓▒░░░▒▒▒▒░░▒▒▒░░▒░░▒▒▓▒▓▒▒▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▒▒░▓███▓▓▓▓░░▓▒█▓▓▓▓▓▒▒▒▒░░▒░░▓▓▓▓▒▒▒▒▒▒▒▒▒▒░░▒░▒▒░░░░░░░░░░░
▒▓░░░░▓▓░░▓▓░▓▒░░▓▒░▒▓▒░░█░▒▓▒█▓▓▒▒▓▒▒█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▒▒░▓████▓██░▒▓▓▓▓▓▓█▓░░░▒░░▒░░▒▓▓▓▒▒▒▒▒▒▒░░▒░░▒░░▒▒░░░░░░░░░▒
▒▓▒▓▓░▓▒░░▒█░█▒░░▒▓░▒▓▒▒░▓▒▓▒▒▓▒▓▒▒▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓▓▒▓████▓██▒▒▓▓▓▓▓▓█▓░░░▒░░▒░░▓▓▓▓▒▒▒▓▒▒▒▒▒▒░░▒░░▒▒▒▒▒▒▒▒▒▒░░
░░░░▓▒▓▓░░▓▓░▓▒░░▓▓░▒▒▒░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓████▒██▒▒▒▓▓▓▓▓█▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒░░░▒▒▒▒▒▒▒▒▒░░
▒▒▒▓▒░░▓▒▓▓░░░▓▒▒▓░░▒▒▒░░░▒░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░░░░▒░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒░░░░░░░▒▒▒▒▒▒▒▒▒▒▒░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░░▒░░░░░░░░░░░░░░░░░
");

On en a tenté un autre pour vous

*/
