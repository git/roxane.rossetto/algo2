#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"part1.h"


MaillonDept lireM(FILE *f)//lire maillon
{
	MaillonDept m;
	fscanf(f, "%s %d", m.departement, &m.nbP);
	fgets(m.respAd, 31, f);
	m.respAd[strlen(m.respAd)-1]='\0';
	return m;
}

ListDept init(void)
{
	return NULL;
}

MaillonDept lireStdin(void)//lire un maillon mais à l'aide de l'entrée standard
{
	MaillonDept m;
	printf("Département :\t");
	fscanf(stdin, "%s", m.departement);
	printf("Nombre de places :\t");
	fscanf(stdin, "%d%*c", &m.nbP);
	printf("Chef de département :\t");
	fgets(m.respAd, 31, stdin);
	m.respAd[strlen(m.respAd)-1]='\0';
	return m;
}

int Exists(char *st1, char *st2)// servira à trouver si le nom de la ville existe déjà donc il suffira d'ajouter un maillon à la chaîne
{
	if (strcmp(st1,st2)==0)
	{
		return 1;//si le nom de la ville existe déjà
	}
}


ListDept InsertT(ListDept list, MaillonDept m)//Insert en tête de la liste
{
	MaillonDept *mins;
	mins = (MaillonDept*)malloc(sizeof(MaillonDept));
	if (mins == NULL){printf("pb malloc"); exit;}
	strcpy(mins->departement, m.departement);
	mins->nbP = m.nbP;
	strcpy(mins->respAd, m.respAd);
	mins->suiv = list;
	return mins;

}


ListDept Insert(ListDept list, MaillonDept m)//insert globalement
{
	if (list == NULL){return InsertT(list, m);}
	if (strcmp(list->departement, m.departement)>0){return InsertT(list, m);}
	list->suiv = Insert(list->suiv, m);
	return list;
}





/*étapes pour le chargement:
1) pour chaque ville : malloc VilleIUT ->initialisation à NULL de la liste et fscanf la ville
2) récup le nb pour faire une boucle
3) boucle dans laquelle on crée nb maillons en inserant département dans la liste en triant
et on recommence tant que fichier non vide*/

int Chargement(VilleIUT **tV, int tmax, char *nameFile)
{
	FILE *f;
	int i = 0, cpt;
	f = fopen(nameFile, "r");
	if (f == NULL){printf("pb ouv file"); return -1;}
	VilleIUT v;

	fscanf(f, "%s %d", v.ville, &v.nbDept);

	while (!feof(f))
	{
		tV[i] = (VilleIUT *)malloc(sizeof(VilleIUT));
		if (tV[i] == NULL){printf("pb malloc");exit;}
		strcpy(tV[i]->ville, v.ville);
		tV[i]->nbDept = v.nbDept;

		cpt = v.nbDept;//le compteur initialisé au nombre de départements

		tV[i]->ldept = init();
		for (cpt; cpt > 0; cpt--)
		{
			MaillonDept m;
			m = lireM(f);//lire le maillon avec la fonction plus haut
			tV[i]->ldept = Insert(tV[i]->ldept, m);//insert le maillon à sa bonne place
		}
		fscanf(f, "%s %d", v.ville, &v.nbDept);
		i++;
	}
	fclose(f);
	return i;


}


void AfficheDpmt ( MaillonDept *dpt )//Affiche un département d'une liste
{
	if ( dpt == NULL) return;
	printf("%s \t%d \t%s \n", dpt->departement, dpt->nbP, dpt->respAd);
	AfficheDpmt(dpt->suiv);
}



void afficheIUT (VilleIUT *iut)//Affiche un iut avec ses départements
{
	printf ("%s \t\n", iut->ville);
	AfficheDpmt(iut->ldept);
}



void Affichetableau(VilleIUT ** TabIUT, int n )//Affiche le tableau des iuts avec leurs départements
{
	int i;
	for ( i = 0 ; i < n ; i++ )
		if ( TabIUT[i]->nbDept > 0)
			afficheIUT(TabIUT[i]);
}

void AffnbPdpt ( MaillonDept *dpt )//Affiche nombre de place par département d'une liste
{
	if ( dpt == NULL) return;
	printf("%s \t%d \n", dpt->departement, dpt->nbP);
	AffnbPdpt(dpt->suiv);
}

void affichenbPIUT (VilleIUT *iut)//Affiche nombre de place restante d'un iut avec ses départements
{
	printf ("%s \n", iut->ville);
	AffnbPdpt(iut->ldept);
}

void AffichenbPtableau(VilleIUT ** TIUT, int n )//Affiche le tableau de places restantes des iuts avec leurs départements
{
	int i;
	for ( i = 0 ; i < n ; i++ )
		if ( TIUT[i]->nbDept > 0)
			affichenbPIUT(TIUT[i]);
}



/*étapes de mise à jour du nombre de places:
demande et recherche dans quelle ville chercher le nombre de places d'un département
chercher ville
chercher dpt
changer nbP
return tV le tableau de VilleIUT mis à jour
*/
void MaJnbP(VilleIUT **tV, int nb)
{
	char ville[31], dpt[31], reponse[31];
	int newnbP;
	int v;
	ListDept elt;

	printf("De quels département et ville souhaitez vous modifier le nombre de place ?\n");
	printf("Ville : \t");
	scanf("%s%*c", ville);

	v = searchVille(tV, ville, nb);

	while(v == -1)//tant que le nom de la ville n'est pas bon
	{
		printf("Ville non trouvée ...Rentrez une autre ville ou tapez 0 pour quitter\n");
		scanf("%s", reponse);
		if (strcmp(reponse, 0)==0)
		{
			exit;
		}
		v = searchVille(tV, reponse, nb);
	}

	printf("Département : \t");
	fgets(dpt, 31, stdin);
	dpt[strlen(dpt)-1] = '\0';

	elt = recherche(tV[v]->ldept, dpt);//on recherche le maillon

	while(elt == NULL)//tant que le nom du département n'est pas bon
	{
		printf("Département non trouvé ...Rentrez un autre département ou tapez 0 pour quitter\n");
		scanf("%s", reponse);
		if (strcmp(reponse, 0)==0)
		{
			exit;
		}
		elt = recherche(tV[v]->ldept, reponse);
	}

	printf("Nouveau nombre de places disponibles : \t");
	scanf("%d", &newnbP);
	elt->nbP = newnbP;//modification du nombre de places
}




ListDept recherche (ListDept l, char *nom )//recherche du nom de département dans la liste
{
	if ( l == NULL ){ return l;}
	if ( strcmp ( l->departement, nom ) == 0) return l;
	return recherche (l->suiv, nom );
}


int searchVille(VilleIUT ** tV, char *ville, int nbV)//recherche du nom de la ville dans le tableau
{
	int i =0;
	for(i; i < nbV; i++)
	{
		if (strcmp(ville, tV[i]->ville)==0)
			return i;
	}
	return -1;//sinon c'est que la recherche n'a pas abouti
}



void createIUT(VilleIUT **tV, int nb)//crée un IUT dans la ville souhaitée.
{
	char reponse[31];
	MaillonDept m;


	int v;//la position de la ville dans le tableau
	char ville[31];//la ville répondue

	printf("Dans quelle ville souhaitez vous ajouter un département?\n");
	scanf("%s%*c", ville);

	v = searchVille(tV, ville, nb);

	while(v == -1)//tant que le nom de la ville n'est pas bon
	{
		printf("Ville non trouvée ...Rentrez une autre ville ou tapez 0 pour quitter\n");
		scanf("%s", reponse);
		if (strcmp(reponse, 0)==0)
		{
			exit;
		}
		v = searchVille(tV, reponse, nb);
	}

	m = lireStdin();//on lit un maillon sur l'entrée standard

	tV[v]->ldept = Insert(tV[v]->ldept, m);//et on l'insert dans la liste du tableau
	tV[v]->nbDept+=1;//on ajoute un au nombre de départements
}


ListDept SuppT(ListDept ld)//supprime la tête d'une liste
{
	MaillonDept *tmp;
	printf("Suppression du maillon\n");
	tmp = ld->suiv;
	free(ld);
	return tmp;
}


ListDept Supp(ListDept ldp, char *nomdpt)//supprime normal
{
	if (ldp == NULL){return ldp;}
	if (strcmp(nomdpt, ldp->departement)==0)return SuppT(ldp);
	if (strcmp(nomdpt, ldp->departement)<0)return ldp;
	ldp->suiv = Supp(ldp->suiv, nomdpt);
	return ldp;
}

void deleteIUT(VilleIUT **tV, int nb)//crée un IUT dans la ville souhaitée.
{
	char reponse[31];
	MaillonDept m;


	int v;//la position de la ville dans le tableau
	char ville[31];//la ville souhaitée
	char tosuppr[31];//la ville à supprimer

	printf("Dans quelle ville souhaitez vous supprimer un département?\n");
	scanf("%s%*c", ville);//la ville

	v = searchVille(tV, ville, nb);

	while(v == -1)//tant que le nom de la ville n'est pas bon
	{
		printf("Ville non trouvée ...Rentrez une autre ville ou tapez 0 pour quitter\n");
		scanf("%s%*c", reponse);
		if (strcmp(reponse, "0")==0)
		{
			exit;
		}
		v = searchVille(tV, reponse, nb);
	}

	printf("Quel département souhaitez-vous supprimer ?\n");
	fgets(tosuppr, 31, stdin);//on récupère dans le tosuppr, le nom de la ville à supprimer
	tosuppr[strlen(tosuppr)-1]='\0';

	tV[v]->ldept = Supp(tV[v]->ldept, tosuppr);//et on la supprime de la liste du tableau
	tV[v]->nbDept-=1;//on enlève un au nombre de départements
}





void MaJnameC(VilleIUT **tV, int nb)//mise à jour du nom du chef de département
{
	char ville[31], dpt[31], reponse[31];//la ville, le département et la réponse
	char newName[31];
	int v;
	ListDept elt;

	printf("De quels département et ville souhaitez vous modifier le nom du chef ?\n");
	printf("Ville : \t");
	scanf("%s%*c", ville);

	v = searchVille(tV, ville, nb);//recherche de la ville

	while(v == -1)//tant que le nom de la ville n'est pas bon
	{
		printf("Ville non trouvée ...Rentrez une autre ville ou tapez 0 pour quitter\n");
		scanf("%s%*c", reponse);
		if (strcmp(reponse, "0")==0)
		{
			exit;
		}
		v = searchVille(tV, reponse, nb);
	}
	printf("Département : \t");//demande le département
	fgets(dpt, 31, stdin);
	dpt[strlen(dpt)-1] = '\0';

	elt = recherche(tV[v]->ldept, dpt);//on recherche le maillon

	while(elt == NULL)//tant que le nom du département n'est pas bon
	{
		printf("Département non trouvé ...Rentrez un autre département ou tapez 0 pour quitter\n");
		scanf("%s", reponse);
		if (strcmp(reponse, 0)==0)
		{
			exit;
		}
		elt = recherche(tV[v]->ldept, dpt);
	}

	printf("Nouveau nom du chef de département : \t");
	fgets(newName, 31, stdin);
	newName[strlen(newName)-1]='\0';
	strcpy(elt->respAd, newName);
}


void SaveEltList(FILE *fs, ListDept l)
{
	fprintf(fs, "%s\t%d\t%s\n", l->departement, l->nbP, l->respAd);
}


void Save(VilleIUT **tV, int nb)//permettra de définir la fermeture de la phase de candidatures
{
	FILE *fs;
	fs = fopen("part1.don", "w");//on réouvre le fichier originel pour y remettre les données
	if (fs == NULL){printf("pb ouv fichier save\n");return;}
	int cpt = 0;
	for(cpt; cpt < nb; cpt ++)
	{
		fprintf(fs, "%s\t%d\n", tV[cpt]->ville, tV[cpt]->nbDept);
		for (int i = 0; i < tV[cpt]->nbDept; i++)
		{
			SaveEltList(fs, tV[cpt]->ldept);
			tV[cpt]->ldept = tV[cpt]->ldept->suiv;
		}
	}
	fclose(fs);
}

int Admin(VilleIUT **tV, int nb)
{
	int reponse;

	printf("*****************************************************\n");
	printf("*------------------Adminitrateur--------------------*\n");
	printf("*-----1-modifier nombre places d'un département-----*\n");
	printf("*--------2-créer un département dans un IUT---------*\n");
	printf("*------3-supprimer un département dans un IUT-------*\n");
	printf("*-----4-modifier le nom du chef de département------*\n");
	printf("*---------5-arrêter la phase de candidatures--------*\n");
	//printf("*--------------------6-Retour-----------------------*\n");
	printf("*****************************************************\n");
	scanf("%d", &reponse);
	switch(reponse)
	{
		case 1: MaJnbP(tV, nb); return 0;
		case 2: createIUT(tV, nb); return 0;
		case 3: deleteIUT(tV, nb); return 0;
		case 4: MaJnameC(tV, nb); return 0;
		case 5: Save(tV, nb); return 1;
		//case 6: exit(1);

	}
}






int utilisateur (VilleIUT **tV, int nb){//Onglet accueille des utilisateurs

int reponse, i;
char rdpt[20];
MaillonDept * ldepart;

	printf("*****************************************************\n");
	printf("*-------------------Utilisateur---------------------*\n");
	printf("*------1-Affichage de toutes les informations-------*\n");
	printf("*-----2-Places restantes pour la première année-----*\n");
	printf("*----------3-Recherche d'un departement-------------*\n");
	printf("*--------------------4-Retour-----------------------*\n");
	printf("*****************************************************\n");


	scanf("%d", &reponse);//enregistrement du choix

	while ( reponse == 1 || reponse == 2 || reponse == 3 || reponse == 4){
		if ( reponse == 1 ){
			printf("Ville/départements \t Places dispo \t\t Responsable\n");
			Affichetableau(tV, nb);
			return 0;
			}
		if ( reponse == 2){
			AffichenbPtableau(tV, nb);
			return 0;
		}
		if ( reponse == 3){
			printf("Quel departement souhaiteriez vous intégrer?\n");
			scanf("%s", rdpt);
			for ( i = 0 ; i < nb ; i++){
				ldepart = recherche (tV[i]->ldept, rdpt);
				if (ldepart != NULL)
				{		printf("%s\n", tV[i]->ville); }
			}
			return 0;
		}
		if ( reponse == 4 )
			return 2;
	}
	return 0 ;
}


void test (void){ // fonction globale

	int acc, nb, nbville, repAdm, repVis;
	VilleIUT *tV[200];

	nbville = Chargement(tV, 200, "part1.don");

	printf("*****************************************************\n");
	printf("*---------------------Accueil-----------------------*\n");
	printf("*-----------------1-Adminitrateur-------------------*\n");
	printf("*-------------------2-Visiteur----------------------*\n");
	printf("*-------------------3-Quitter-----------------------*\n");
	printf("*****************************************************\n");
	scanf("%d", &acc);

	while ( acc == 1 || acc == 2){

		if (acc == 1){
				repAdm = Admin (tV, nbville);

		}
		if (acc == 2){
			repVis = utilisateur (tV, nbville);

			if (repVis == 2){
					acc = 3;
			}
		}
	if ( acc == 3 ){
		Save( tV, nb );
		exit;
	}
	return;
	}
}
