#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "part2.h"


//##########################TABLEAU POINTEUR#####################################################


Choix lirechx (void){//Fonction qui va lire les choix de la ville et du departement demandé par le candidat
	Choix chx;

	printf("Saisir le choix de la ville \n");
	scanf( "%s", chx.ville);

	printf("Saisir le choix du departement d'étude\n");
	scanf("%s" , chx.dptmt);

	chx.dec = 0;
	chx.valid = 0;
	return chx;
}


Listechx init(void)
{
	return NULL;
}

Listecand initCand(void)
{
	return NULL;
}

Choix lireC(FILE *fe)
{
	Choix c;
  fscanf(fe, "%s%*c", c.ville);
	fgets(c.dptmt, 26, fe);
	c.dptmt[strlen(c.dptmt)-1]= '\0';
	fscanf(fe, "%d %d%*c", &c.dec, &c.valid);
	return c;
}



Candidat lireCandidat(void)//fonction qui lit un candidat sur le clavier
{
	Candidat c;

	printf("Veuillez entrer votre numéro de Candidat");
	scanf("%d%*c", &c.nEtu );
	printf("Nom du candidat :\n");
	fgets(c.nom, 23, stdin);
	c.nom[strlen(c.nom)-1]= '\0';
	printf("Prénom du candidat :\n");
	fgets(c.prenom, 23, stdin);
	c.prenom[strlen(c.prenom)-1]= '\0';
	printf("Moyenne de maths :\n");
	scanf("%f", &c.moymat);
	printf("Moyenne de français :\n");
	scanf("%f", &c.moyfr);
	printf("Moyenne d'anglais :\n");
	scanf("%f", &c.moyen);
	printf("Moyenne de spécialité au choix :\n");
	scanf("%f", &c.moyspe);
	printf("Nombre de choix :\n");
	scanf("%d%*c", &c.nbchx);
	return c;
}


Candidat lireCa(FILE *fe)
{
	Candidat c;//candidat que l'on retournera
	Choix chx;//pour la création de maillons
	int cpt = 0;//qui servira à compter le nombre de choix
	fscanf(fe,"%d", c.nEtu);
	fgets(c.nom, 23, fe);
	c.nom[strlen(c.nom)-1]= '\0';
	fgets(c.prenom, 23, fe);
	c.prenom[strlen(c.prenom)-1]= '\0';
	fscanf(fe, "%f %f %f %f %d%*c", c.moymat, c.moyfr, c.moyen, c.moyspe, c.nbchx);
	c.lchx = init();//initialisation de la liste

	return c;
}



Listechx InsertTC(Listechx list, Choix m)//Insert en tête de la liste
{
	Maillonchx *mchx;
	mchx = (Maillonchx*)malloc(sizeof(Maillonchx));
	if (mchx == NULL){printf("pb malloc"); exit(-1);}
	mchx->chx = m;
	mchx->suivchx = list;
	return mchx;
}



Listechx InsertC(Listechx list, Choix m)//insert globalement dans liste de choix
{
	if (list == NULL){return InsertTC(list, m);}
	if (strcmp(list->chx.dptmt, m.dptmt)>0){return InsertTC(list, m);}
	list->suivchx = InsertC(list->suivchx, m);
	return list;
}


void AffC(Candidat c)
{
	printf("%d\n", c.nEtu);
	printf("%s\n%s\n", c.nom, c.prenom);
	printf("%.2f\t%.2f\t%.2f\t%.2f\n", c.moymat,c.moyfr, c.moyen, c.moyspe);
	printf("%d\n", c.nbchx);
}

void affnomcand (Candidat c){
	printf("%s\n%s\n", c.nom, c.prenom);
}

void AffCh(Choix c)
{
	printf("%s \n", c.ville);
	printf("%s \n", c.dptmt);
	printf("%d \n", c.dec);
	printf("%d \n", c.valid);
}

//######################################### LISTES ##########################################



void AffListeCandidat (Listecand l){

	if (l == NULL) return;
	AffC(l->cand);
	if (l->cand.lchx == NULL){printf("whybitch");}
	AffListChoix(l->cand.lchx);
	AffListeCandidat(l->suivcand);
}

void AffCandid (Listecand l){
	if ( l == NULL ) return;
	affnomcand( l->cand);
	AffCandid ( l->suivcand);
}

void AffListChoix(Listechx l)
{
	if (l == NULL) return;
	AffCh(l->chx);
	AffListChoix(l->suivchx);
}

Listecand recherchenom (Listecand l, char *nom ){
	if ( l == NULL ){ return l;}
	if ( strcmp ( l->cand.nom, nom ) == 0) return l;
	return recherchenom (l->suivcand, nom );
}

Listecand recherchenum (Listecand l, int num){
	if ( l == NULL ){ return l;}
	if ( l->cand.nEtu == num ) return l;
	return recherchenum (l->suivcand, num );
}

Listechx recherchechx (Listechx l, char *nom )//recherche du nom de département dans la liste
{
	if ( l == NULL ){ return l;}
	if ( strcmp ( l->chx.dptmt, nom ) == 0) return l;
	return recherchechx (l->suivchx, nom );
}

Listechx searchChx(Listechx lchx, char *ville, char *dptmt)
{
	if (lchx == NULL){return lchx;}
	if (strcmp(lchx->chx.ville, ville)==0 && strcmp(lchx->chx.dptmt, dptmt)==0)return lchx;
	return searchChx(lchx->suivchx, ville, dptmt);
}

Listecand recherchevilledpt ( Listecand l, char *ville, char *dpt){
	Listecand lca;
	Listechx lch;

	lch = init();
	lca = initCand();

	while ( l !=NULL ){
		lch = searchChx(l->cand.lchx, ville, dpt);
		if(lch != NULL){
			lca = Insert (lca, l->cand);
		}
		l = l->suivcand;
	}
	 return lca;
}

Listechx majchoix (Listechx chx, Choix c){//Mise à jour du de la liste choix en modifiant un maillon

	strcpy(chx->chx.ville, c.ville);
	strcpy (chx->chx.dptmt, c.dptmt);
	chx->chx.dec = c.dec;
	chx->chx.valid = c.valid;

	return chx; 
}

Listecand InsertT(Listecand list, Candidat c)//Insert en tête de la liste
{
	Mailloncand *c1;

	c1 = (Mailloncand*)malloc(sizeof(Mailloncand));
	if (c1 == NULL){printf("pb malloc"); exit;}
	c1->cand.nEtu = c.nEtu;
	strcpy(c1->cand.nom, c.nom);
	strcpy(c1->cand.prenom, c.prenom);
	c1->cand.moymat = c.moymat;
	c1->cand.moyfr = c.moyfr;
	c1->cand.moyen = c.moyen;
	c1->cand.moyspe = c.moyspe;
	c1->cand.nbchx = c.nbchx;
	c1->cand.lchx = c.lchx;
	c1->suivcand = list;
	return c1;
}


Listecand Insert(Listecand list, Candidat c)//insert globalement
{
	if (list == NULL){return InsertT(list, c);}
	if (strcmp(list->cand.nom, c.nom)>0){return InsertT(list, c);}
	list->suivcand = Insert(list->suivcand, c);
	return list;
}



Listecand Chargementlistecandidat(FILE *fe, Listecand lC, int *nbC)// fonction de chargement de la liste des candidats
{
	int cpt, j;
	fscanf(fe, "%d", nbC);
	for (j = 1; j <= *nbC; j++)
	{
    Candidat Ca;
		fscanf(fe, "%d %s%*c", &Ca.nEtu, Ca.nom);
  	fgets(Ca.prenom, 31, fe);
  	Ca.prenom[strlen(Ca.prenom)-1]= '\0';
    fscanf(fe,"%f %f %f %f %d%*c", &Ca.moymat, &Ca.moyfr, &Ca.moyen, &Ca.moyspe, &Ca.nbchx);
    Ca.lchx=NULL;

		for (cpt = Ca.nbchx; cpt >= 1; cpt--)
		{
			Maillonchx chx;
			Choix c;
			c = lireC(fe);//lire le maillon avec la fonction plus haut
			chx.chx = c;//le choix du maillon est celui lu au dessus
			Ca.lchx = InsertC(Ca.lchx, c);//insert le maillon à sa bonne place
		}
		lC = Insert(lC, Ca);
	}
	fclose(fe);
	return lC;
}

Listechx suppressionchxT (Listechx l){//Fonction suppression d'un maillon
	Listechx tmp;
	tmp = l->suivchx;
	free(l);
	return tmp;
}

Listechx suppressionchx(Listechx l , char *depart){//suppression d'un maillon viale nom de son departement
	if ( l == NULL) return l;
	if ( strcmp ( depart, l->chx.dptmt ) == 0 )
		return suppressionchxT(l);
	l->suivchx = suppressionchx( l->suivchx , depart);
	return l;
}



void Save(Listecand lC, int nbC)
{
	FILE *fs;
	fs=fopen("part2.don", "w");
	if (fs == NULL){printf("pb ouv fichier part2.don\n");exit(-1);}
	int cpt;
	fprintf(fs, "%d\n", nbC);

	saveC(lC, fs);
}

void saveC(Listecand lC, FILE *fs)
{
	if (lC == NULL)return;
	fprintf(fs, "%d\n", lC->cand.nEtu);
	fprintf(fs, "%s\n", lC->cand.nom);
	fprintf(fs, "%s\n", lC->cand.prenom);
	fprintf(fs, "%.2f\t %.2f\t %.2f\t %.2f\n", lC->cand.moymat, lC->cand.moyfr, lC->cand.moyen, lC->cand.moyspe);
	fprintf(fs, "%d\n", lC->cand.nbchx);
	saveChx(lC->cand.lchx, fs);
	saveC(lC->suivcand, fs);
}



void saveChx(Listechx lCh, FILE *fs)
{
	if (lCh == NULL)return;
	fprintf(fs, "%s\n", lCh->chx.ville);
	fprintf(fs, "%s\n", lCh->chx.dptmt);
	fprintf(fs, "%d\n", lCh->chx.dec);
	fprintf(fs, "%d\n", lCh->chx.valid);
	saveChx(lCh->suivchx, fs);
}



void test (void){

	int menu1, menugest1, numet,modifcand, cont = 0, nbC;
	char nomet[20], nomdpt[20], nomville[20];
	Candidat cand1;
	Choix choix1;
	Listecand lcand, l2cand;
	Listechx chx;
	FILE *fe;

	fe = fopen ("part2.don", "r");
	if ( fe == NULL ){printf("prob ouverture fichier\n"); return;}


	lcand = Chargementlistecandidat(fe, lcand, &nbC);

	printf("Menu :\n");//Premier affichage menu pour choisir si on veut afficher le menu d'un candidat ou toutes les candidatures
	printf("(1) Gestion Candidature\n(2) Affichage Candidature\n");
	scanf("%d", &menu1);

	while (menu1 != 1 && menu1 !=2 && menu1 !=3){// Traitement des erreurs
		printf("Erreur de saisie veuillez refaire votre choix ! \n");
		printf("(1) Gestion Candidature\n");
		printf("(2) Affichage Candidature\n");
		printf("(3) Quitter\n");
		scanf("%d", &menu1);
	}

		if (menu1 == 1){
			printf("Menu : \n");//Menu gestion ajout/modification/suppression d'une candidature
			printf("(1) Ajouter une Candidature\n (2) Modifier une Candidature\n (3) Supprimer une Candidature");
			scanf("%d", &menugest1);

			if (menugest1 == 1){// Ajout d'une candidature (choix) a un candidat deja existant
				printf("Veuillez renseigner votre nom d'étudiant\n");
				scanf("%s", nomet);
				l2cand = recherchenom ( lcand, nomet );

				choix1 = lirechx();
				lcand->cand.lchx = InsertTC (l2cand->cand.lchx,choix1);
				AffListeCandidat(lcand);
			}


			else if (menugest1 == 2){//
				printf("Veuillez renseigner votre nom d'étudiant\n");
				scanf("%s", nomet);
				l2cand = recherchenom ( lcand, nomet );

				printf("Entrer le nom du departement de l candidature concernée\n");
				scanf("%s", nomdpt);
				l2cand->cand.lchx = recherchechx(l2cand->cand.lchx, nomdpt);
				while ( l2cand->cand.lchx->chx.dptmt == NULL){
					printf("departement inconnu veuillez saisir à nouveau\n");
					scanf("%s", nomdpt);
				l2cand->cand.lchx = recherchechx(l2cand->cand.lchx , nomdpt);
				}
				choix1 = lirechx();

				l2cand->cand.lchx = majchoix(l2cand->cand.lchx, choix1);
				AffListeCandidat(l2cand);	
			}


			else if (menugest1 ==  3){
				printf("Veuillez renseigner votre nom d'étudiant\n");
				scanf("%s", nomet);
				l2cand = recherchenom ( lcand, nomet );

				printf("Entrer le nom du departement que vous souhaitez supprimer\n");
				scanf ("%s", nomdpt);
				l2cand->cand.lchx = suppressionchx(l2cand->cand.lchx , nomdpt );
				printf("Voici votre nouvelle liste de candidature\n");
				AffListeCandidat(l2cand);
			}

		}
		else if (menu1 == 2){


			printf("Entrer le nom de la ville dans laquelle se trouve le departement d'étude\n");
			scanf("%s", nomville);

			printf("Entrer le nom du departement dont vous souhaitez afficher les candidats\n");
			scanf("%s", nomdpt);

			l2cand = recherchevilledpt ( lcand, nomville, nomdpt);

			AffCandid (l2cand);
		}
		else if (menu1 == 3){
			Save(lcand, nbC);
			fclose(fe);
			return;
		}

Save(lcand, nbC);
fclose(fe);
}




//####################################### FILES #################################################
