/*Fichier destiné à accueillir la partie 1 de la SAE de structure de données.
Dans cette partie nous allons implémenter un tableau de pointeurs dans lequel nous retrouverons des strucutures de type ville qui comprennent le nom et un pointeur vers la liste de maillons de départments.
Pour l'instant cela semble difficile mais nous allons diviser le travail de sorte à d'abord créer les structures requises, donc ici VilleIut et la liste. Nous ferons ensuite correspondre à ces structures, des données que nous pourrons insérer via des fonctions de type lireDep.. triées en fonction d'algorithmes de tris qui permettront plus tard une recherche simplifiée.
Etant donné qu'il sera nécessaire de faire 2 parties (une partie administrateur et une partie utilisateur) nous allons en prendre une chacune : Roxane la partie utilisateur et Lola la partie administrateur.*/




typedef struct list{
	char departement[31];//le nom du département
	int nbP;//nombre de places
	char respAd[31];//nom du responsable
	struct list * suiv;//pointeur suivant de la liste
}MaillonDept;


typedef struct{
	char ville[30];//nom de la ville
	int nbDept;//nombre de départements
	MaillonDept * ldept;//liste de départements
}VilleIUT;

typedef MaillonDept * ListDept;


ListDept init(void);//fontion initialisation
MaillonDept lireM(FILE *f);//lire maillon
int Exists(char *st1, char *st2);// servira à trouver si le nom de la ville existe déjà donc il suffira d'ajouter un maillon à la chaîne
ListDept InsertT(ListDept list, MaillonDept m);//Insert en tête de la liste
ListDept Insert(ListDept list, MaillonDept m);//insert globalement
int Chargement(VilleIUT **tV, int tmax, char *nameFile);//charge le fichier dans le tableau
void AfficheDpmt ( MaillonDept *dpt );//Affiche un département d'une liste
void afficheIUT (VilleIUT *iut);//Affiche un iut avec ses départements
void Affichetableau(VilleIUT ** TabIUT, int n );//Affiche le tableau des iuts avec leurs départements
ListDept recherche (ListDept l, char *nom );//recherche du nom de département dans la liste
void MaJnbP(VilleIUT **tV, int nb);//mise à jour du nombre de places
int searchVille(VilleIUT ** tV, char *ville, int nbV);//recherche du nom de la ville dans le tableau
void createIUT(VilleIUT **tV, int nb);//crée un IUT dans la ville souhaitée.
MaillonDept lireStdin(void);
ListDept SuppT(ListDept ld);//supprime la tête d'une liste
ListDept Supp(ListDept ldp, char *nomdpt);//supprime normal
void deleteIUT(VilleIUT **tV, int nb);//crée un IUT dans la ville souhaitée.
void MaJnameC(VilleIUT **tV, int nb);//mise à jour du nom du chef de département
void Save(VilleIUT **tV, int nb);//sauvegarde le fichier
void SaveEltList(FILE *fs, ListDept l);//sauvegarde un élément de liste
int Admin(VilleIUT **tV, int nb);//partie admin de la première partie
void AffnbPdpt ( MaillonDept *dpt );//Affiche nombre de place par département d'une liste
void affichenbPIUT (VilleIUT *iut);//Affiche nombre de place restante d'un iut avec ses départements
void AffichenbPtableau(VilleIUT ** TIUT, int n );//Affiche le tableau de places restantes des iuts avec leurs départements
int utilisateur (VilleIUT **tV, int nb);//Onglet accueille des utilisateurs
void test (void); // fonction globale
