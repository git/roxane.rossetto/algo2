/*
  programme : commun.h
  auteur : Lola Chalmin, Roxane Rossetto
  date : 13/12/22
  finalité : SAE 1.02 Algorithme
*/
/*
  Brief :
*/
#define TAILLE 100;


typedef struct {
  char ville[20];//ville choisie
  char dptmt[20];//département choisi
  int dec;//décision du choix
  int valid;//validation du candidat
}Choix;


typedef struct Listchx{
  Choix chx;
  struct Listchx * suivchx;
}Maillonchx;

typedef Maillonchx * Listechx;


typedef struct {
  int nEtu;//numéro d'étudiant
  char nom[20];//nom de l'étudiant
  char prenom[20];//prénom de l'étudiant
  float moymat;//moyenne en maths
  float moyfr;//moyenne en français
  float moyen;//moyenne en anglais
  float moyspe;//moyenne en spécialité
  int nbchx;//nombre de choix
  Listechx lchx;
}Candidat;


typedef struct Mailloncand{
  Candidat cand;
  struct Mailloncand * suivcand;
}Mailloncand;

typedef Mailloncand * Listecand;


Listechx init(void);
Listecand initCand(void);
Choix lirechx (void);//permet de lire un choix selon une entrée stdin
Choix lireC(FILE *fe);//ermet de lire un choix avec un FILe de type fichier.don
Candidat lireCandidat(void);//permet de lire un candidat avec le clavier
Candidat lireCa(FILE *fe);//permet de lire un candidat selon le fichier.don
Listechx InsertTC(Listechx list, Choix m);//insertion en tête d'un maillon de type choix
Listechx InsertC(Listechx list, Choix m);//insertion globale d'un maillon de type choix


void AffC(Candidat c);//affichage simple d'un candidat
void AffCh(Choix c);//affichage simple d'un choix
void affnomcand (Candidat c);
void AffListeCandidat (Listecand l);//affiche complet de la liste de candidats
void AffCandid (Listecand l);//affiche que le nom
void AffListChoix(Listechx l);//affichage complet de la liste de choix
Listecand recherchenom (Listecand l, char *nom );//recherche par un nom dans une liste de candidats
Listecand recherchenum (Listecand l, int num);//recherche par le numéro dans une liste de candidats
Listechx searchChx(Listechx lchx, char *ville, char *dptmt);
Listechx recherchechx (Listechx l, char *nom );// recherche par le nom d'un depratement dans une listedederpartement
Listecand recherchevilledpt ( Listecand l, char *ville, char *dpt);//recherche et retour de la liste des candidats ayant postuler a un certain departement
Listechx majchoix (Listechx chx, Choix c);// mise  jour d'un choix
Listecand Chargementlistecandidat(FILE *fe, Listecand lC, int *nbC);//chargement du fichier dans une liste de candidats
Listechx suppressionchxT (Listechx l);//Fonction suppression d'un maillon
Listechx suppressionchx(Listechx l , char *depart);//suppression d'un maillon viale nom de son departement
Listecand Insert(Listecand list, Candidat c);//insertion en tête d'un maillon de type Candidat
Listecand InsertT(Listecand list, Candidat c);//insertion globale d'un maillon de type Candidat
void test (void);//fonction globale
void testchgt(void);
void saveC(Listecand lC, FILE *fs);
void Save(Listecand lC, int nbC);
void saveChx(Listechx lCh, FILE *fs);
