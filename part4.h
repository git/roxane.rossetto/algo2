typedef struct {
  char ville[26];//ville choisie
  char dptmt[26];//département choisi
  int dec;//décision du choix
  int valid;//validation du candidat
}Choix;


typedef struct Listchx{
  Choix chx;
  struct Listchx * suivchx;
}Maillonchx;

typedef struct {
  Maillonchx *ttchx;
  Maillonchx *kechx;} Filechx;


typedef struct {
  int nEtu;//numéro d'étudiant
  char nom[26];//nom de l'étudiant
  char prenom[26];//prénom de l'étudiant
  float moymat;//moyenne en maths
  float moyfr;//moyenne en français
  float moyen;//moyenne en anglais
  float moyspe;//moyenne en spécialité
  float noteDoss;//note après étude du dossier
  int nbchx;//nombre de choix
  Filechx fchx;
}Candidat;


typedef struct Mailloncand{
  Candidat cand;
  struct Mailloncand * suivcand;
}Mailloncand;

typedef struct {
  Mailloncand *ttcand;
  Mailloncand *kecand;} Filecand;



Filechx initchx (void);// initialise une file choix a NULL
Filecand initcand (void);// initialise une file candidat a NULL
int testVideCand (Filecand f);// fonction test si la Filecand est vide return 1 sinon 0
Candidat lireCa(FILE *fe);//Lire une structure candidat a partir d'un fichier
//Filechx enfilechx ( Filechx f , File *fe);//fonction enfile un maillon choix en fin de file Choix
//Filecand enfilecand (Filecand f, FILE *fe);// fonction enfile un maillon cand en fin de file Candidat
Choix lireC(FILE *fe);//fonction extraite de la partie 2
Filecand enfC(Filecand fC, Candidat c);
Filechx enf(Filechx fchx, Choix c);
Choix def(Filechx fc);
Candidat defC(Filecand fC);
int chargement (Candidat *tCandAcc[], int tmax, FILE *fe);// chargement des files cand et chx
Filecand ChargementfileC(Filecand fC, int *nbC);//chargement pour la file d'attente

void test (void);


